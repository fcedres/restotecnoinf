#include "Mozo.h"

Mozo::Mozo() : Empleado(){
    this->colMesas;//.clear();
}


Mozo::Mozo(int idEmpleado, string nombre):Empleado(idEmpleado, nombre){
	this->colMesas;
}

/*int Mozo::getIdEmpleado(){
    return idEmpleado;
}

string Mozo::getNombre(string nombre){
    return nombre;
}

void Mozo::setIdEmpleado(int IdEmpleado){
    this->IdEmpleado=IdEmpleado;
}


void Mozo::setNombre(string nombre){
    this->nombre=nombre;
}
*/

Mozo::~Mozo()=default;

list<int> Mozo::obtenerMesasAsignadasSVC(){
	map<int,Mesa*> aux = this->colMesas;
	list<int> resultado;
	int idMesa;

	for(map<int,Mesa*>::iterator it=aux.begin(); it != aux.end(); ++it){
		if (it->second->tieneVentaEnCurso()==false){
			idMesa = it->second->getNumero();
			resultado.push_front(idMesa);
		}
	}
	return resultado;
}

void Mozo::agregarMesa(Mesa* m){

	int idMesa = m->getNumero();

	//Mesa* mesa = new Mesa()
	this->colMesas[idMesa]=m;

}
