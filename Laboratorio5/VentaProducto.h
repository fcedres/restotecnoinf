/*
 * VentaProducto.h
 *
 *  Created on: 18/06/2020
 *      Author: eduardo
 */

#ifndef VENTAPRODUCTO_H_
#define VENTAPRODUCTO_H_

#include "Producto.h"

class VentaProducto {
private:
	int cantProdVenta;
	Producto* producto;
public:
	VentaProducto();
	~VentaProducto();
	int obtenerIdProducto();
	bool existeProductoVP(int idProducto);
	void confirmarBajaProducto(int idProducto);
};

#endif /* VENTAPRODUCTO_H_ */
