#ifndef Laboratorio5_Venta_h
#define Laboratorio5_Venta_h

#include <map>
#include <string>
#include "Factura.h"
#include "VentaProducto.h"
#include <list>

using namespace std;

class Venta{
private:
	int idVenta;
	Factura* factura;
	list<VentaProducto*> colVentaProducto;

public:
	//constructor
	Venta();
	Venta(int idVenta, Factura* factura);

	//seters
	int getidVenta();

	//geters
	void setidVenta(int idVenta);
	bool tieneFactura();
	void eliminarProducto(int IdProducto);
    bool existeProducto(int IdProducto);

	//destructor
	~Venta();
};



#endif
