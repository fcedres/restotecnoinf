//
// Created by Fabio on 9/6/2020.
//
class Menu;
#ifndef LABORATORIO5_MENUCOMUN_H
#define LABORATORIO5_MENUCOMUN_H

#include "Comun.h"
#include "Menu.h"
#include "./DataTypes/DtProducto.h"
#include "./DataTypes/DtComun.h"
#include <map>
#include <string>
using namespace std;

class MenuComun{

private:
    int cantProdMenu;
    Comun* comun;
    Menu* menu;
public:
    //Constructores

    MenuComun();
    MenuComun(int cantProdMenu);
    //MenuComun(int cantProdMenu, Comun* comun, Menu* menu);

    //Seters

    void setCantProdMenu(int cantProdMenu);


    //Geters
    int getCantProdMenu();


    map<int, DtComun*> obtenerProductosMenu();

    string obtenerNombreProducto();

    //Destructor
    ~MenuComun();
};
#endif //LABORATORIO5_MENUCOMUN_H
