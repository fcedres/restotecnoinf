/*
 * CVentaFacturaMesa.h
 *
 *  Created on: 12/06/2020
 *      Author: Alejandro
 */

#ifndef Laboratorio5_CVentaFacturaMesa_h
#define Laboratorio5_CVentaFacturaMesa_h

#include "../Interfaces/ICVentaFacturaMesa.h"
#include "../DataTypes/DtFactura.h"
#include "../DataTypes/DtCliente.h"
#include "../Venta.h"
#include "../Mesa.h"
#include "../Domicilio.h"
#include <list>
#include <map>
#include <iostream>
#include <stdlib.h> //para usar funciones de numeros random
using namespace std;

class CVentaFacturaMesa: public ICVentaFacturaMesa{
private:
    static CVentaFacturaMesa* instancia;
    CVentaFacturaMesa();
    //CVentaFacturaMesa(CVentaFacturaMesa const&){};
    int idVenta;
    int numero;
    int idProducto;
    int cantidad;
    Venta* venta;
    Mesa* mesa;
    map<int, Venta*> colVentas;
    map<int, Mesa*> colMesas;
public:
    // operaciones del controlador
    static CVentaFacturaMesa* getInstancia();

    //operaciones del sistema
//    void disminuircantidad();
//    int obtenercantidadproductos ();
//    list<int> listarVentasNofacturadas ();
//    void incrementarcantidad();
//    void seleccionarVenta(int idVenta);
//    //list<int> ingresarVentaMesa(int idEmpleado);
//    list<int> listarMesas ();
//    void quitarProducto();
//    list<int> listarProductosDeLaVenta();
//    void seleccionarMesa(int numero);
//    void agregarProductoaMesa();
//    map<int, Factura*> ventaMesa(DtFactura idMesa,DtFactura descuento);
    int iniciarVenta(list<int> numero);
    int iniciarVentaDomicilio(DtCliente cliente);
    bool confirmarBajaProducto(int idProducto);
    void crearMesa(int numero);
    map<int,Mesa*> obtenerMesas();
    void asignarMozoAMesa();
//    list<int> obtenerVentas();
    //destructor
    ~CVentaFacturaMesa();


};


#endif //LABORATORIO5_CVENTAFACTURAMESA_H
