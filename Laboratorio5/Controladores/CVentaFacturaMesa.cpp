/*
 * CVentaFacturaMesa.cpp
 *
 *  Created on: 12/06/2020
 *      Author: Alejandro
 */

 #include "CVentaFacturaMesa.h"

CVentaFacturaMesa* CVentaFacturaMesa::instancia = nullptr;

CVentaFacturaMesa::CVentaFacturaMesa(){
	this->colMesas.clear();
	this->colVentas.clear();
}

CVentaFacturaMesa* CVentaFacturaMesa::getInstancia(){

    if(instancia==nullptr){
        instancia = new CVentaFacturaMesa();
    }
    return instancia;
}

int CVentaFacturaMesa::iniciarVenta(list<int> numero){//iniciarVenta(list<int> numero){

	int idVenta = colVentas.size()+1; // genero el Nro de venta "aleatorio"
	Local* venta = new Local(); // se crea la instancia de la venta
	venta->setidVenta(idVenta);
	colVentas[idVenta]=venta;

	for(list<int>::iterator it=numero.begin(); it!=numero.end(); it++){
		int numero = *it;
		colMesas.find(numero)->second->setLocal(venta);
	}
	return idVenta;
}

int CVentaFacturaMesa::iniciarVentaDomicilio(DtCliente cliente){

	int idVenta = colVentas.size()+1; // genero el Nro de venta "aleatorio"
	//Local* venta = new Local();
	Domicilio* dom = new Domicilio();
	dom->setidVenta(idVenta);
	colVentas[idVenta]=dom;
	return idVenta;
}

bool CVentaFacturaMesa::confirmarBajaProducto(int idProducto){

	bool existe=false;

	int idP = idProducto;
	list<int> idVenta;
	idVenta.clear();

	map<int,Venta*> aux = this->colVentas;

	for (map<int,Venta*>::iterator it=aux.begin(); it!= aux.end(); ++it){
		bool b = it->second->tieneFactura();

		if (!b){
			bool existe=false;
			existe= it->second->existeProducto(idP);
		}
	}
	return existe;
}

void CVentaFacturaMesa::crearMesa(int numero){
	Mesa* m = new Mesa();
	m->setNumero(numero);//(numero,nullptr);
	this->colMesas[numero]=m;
}

map<int,Mesa*> CVentaFacturaMesa::obtenerMesas(){
	map<int,Mesa*> aux = this->colMesas;
	return aux;
}

//list<int> CVentaFacturaMesa::obtenerVentas(){
//}

void CVentaFacturaMesa::asignarMozoAMesa(){

}
