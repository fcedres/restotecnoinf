//
// Created by Fabio on 3/6/2020.
//

#include "CPersona.h"

CPersona* CPersona::instancia = nullptr;

CPersona::CPersona(){
	this->colClientes.clear();// new map<int,Cliente*>;
	this->colEmpleados.clear();//=new map<int,Empleado*>;
}

CPersona* CPersona::getInstancia(){
    if(instancia==nullptr){
        instancia = new CPersona();
    }
    return instancia;
}

list<int> CPersona::ingresarVentasMesa(int idEmpleado){
	list<int> mesasObtenidas;

	Empleado* e = colEmpleados.find(idEmpleado)->second; //encuentra al empleado

	Mozo* mozo = dynamic_cast<Mozo*>(e);
	Repartidor* rep = dynamic_cast<Repartidor*>(e);

	if (mozo){
		mesasObtenidas = mozo->obtenerMesasAsignadasSVC();
	}
	return mesasObtenidas;
}

void CPersona::altaEmpleado(DtEmpleado* empleado){
	const int id = empleado->getIdEmpleado();
	string nombre = empleado->getNombre();

	DtMozo* mozo =  dynamic_cast<DtMozo*>(empleado);
	DtRepartidor* repartidor = dynamic_cast<DtRepartidor*>(empleado);

	if (mozo){
		Empleado* mozo = new Mozo(id, nombre);
		this->colEmpleados[id]=mozo;//.insert(make_pair(id,mozo));
	}

	if (repartidor){
		tipoTransporte transporte = repartidor->getTransporte();
		Empleado* rep = new Repartidor(id, nombre, transporte);
		this->colEmpleados[id]=rep;//.insert(make_pair(id,rep));
	}
}

map<int,DtEmpleado*> CPersona::listarEmpleados(){

	map<int,DtEmpleado*> listadoResultado;
	int cantidad2;
	for(map<int,Empleado*>::iterator it=this->colEmpleados.begin(); it!=this->colEmpleados.end(); ++it){

		Empleado* emp = it->second;
		//this->colEmpleados.erase(emp->getIdEmpleado());
		Mozo* mozo = dynamic_cast<Mozo*>(emp);
		Repartidor* rep = dynamic_cast<Repartidor*>(emp);


		if (mozo){
			int id = it->second->getIdEmpleado();

			DtEmpleado* mozos = new DtMozo(mozo->getIdEmpleado(),mozo->getNombre());
			listadoResultado[id]=mozos;// .insert(make_pair(id,mozos));
		}
		else if (rep){
			int id = it->second->getIdEmpleado();
			DtEmpleado* repartidor = new DtRepartidor(rep->getIdEmpleado(),rep->getNombre(),rep->getTransporte());
			listadoResultado[id]=repartidor;//.insert(make_pair(id,repartidor));

		}

	}
	return listadoResultado;
}

void CPersona::cancelar(){}

int CPersona::obtenerTamanioColeccionEmpleado(){
	return this->colEmpleados.size();
}

void CPersona::altaCliente(DtCliente* cliente){

		string nombre = cliente->getNombre();
		int telefono = cliente->getTelefono();
		string calle = cliente->getDireccion()->getNombreCalle();
		int numero = cliente->getDireccion()->getNumeroPuerta();
		string esq1 = cliente->getDireccion()->getEsquina1();
		string esq2 = cliente->getDireccion()->getEsquina2();

		DtDireccion* dire =  dynamic_cast<DtDireccion*>(cliente->getDireccion());
		DtApartamento* apto = dynamic_cast<DtApartamento*>(cliente->getDireccion());


		if (apto){
			string edi = apto->getNombreEdificio();
			int apartamento = apto->getNroApto();

			DtDireccion* apar = new DtApartamento(calle,numero,esq1,esq2,edi,apartamento);
			Cliente* cliA = new Cliente(telefono,nombre,apar);
			this->colClientes[telefono]=cliA;
		}
		else if(dire){
			DtDireccion* direc = new DtDireccion(calle,numero,esq1,esq2);
			Cliente* cli = new Cliente(telefono, nombre,direc);
			this->colClientes[telefono]=cli;
		}

}

map<int,DtCliente*> CPersona::listarClientes(){
	map<int,Cliente*> cli = this->colClientes;

	map<int,DtCliente*> listadoResultado;

	for(map<int,Cliente*>::iterator it=this->colClientes.begin(); it!=this->colClientes.end(); ++it){

		DtDireccion* direccion = it->second->getDireccion();
		string nombre = it->second->getNombre();
		int telefono = it->second->getTelefono();

		DtDireccion* dire = dynamic_cast<DtDireccion*>(direccion);
		DtApartamento* apar = dynamic_cast<DtApartamento*>(direccion);

		if (apar){
			DtDireccion* dir = new DtApartamento(apar->getNombreCalle(),apar->getNumeroPuerta(),apar->getEsquina1(),
					apar->getEsquina2(),apar->getNombreEdificio(),apar->getNroApto());
			DtCliente* clienteA = new DtCliente(telefono,nombre,dir);
			listadoResultado[telefono]=clienteA;
		}
		else if (dire){
			DtDireccion* dir = new DtDireccion(dire->getNombreCalle(),dire->getNumeroPuerta(),dire->getEsquina1(),dire->getEsquina2());
			DtCliente* clienteC = new DtCliente(telefono,nombre,dir);
			listadoResultado[telefono]=clienteC;
		}
		nombre.clear();
		telefono=0;
	}
	return listadoResultado;
}

void CPersona::asignarMesaAMozo(int idMozo, Mesa* m){
	Empleado* emp = this->colEmpleados.find(idMozo)->second;

	Mozo* mozo = dynamic_cast<Mozo*>(emp);

	if (mozo){

		mozo->agregarMesa(m);
	}



}



CPersona::~CPersona(){}
