//
// Created by Fabio on 3/6/2020.
//

#include "CProducto.h"
#include "../Menu.h"
#include "../Comun.h"
//Puntero a CinePeliculaController vacío y

CProducto* CProducto::instancia = nullptr;

CProducto::CProducto(){
	this->colProductos.clear();
}

CProducto* CProducto::getInstancia(){
    if(instancia==nullptr){
       instancia = new CProducto();
    }
    return instancia;
}

DtMenuVendido CProducto::crearMenu(int codigo, string descripcion){
	this -> codigo = codigo;
    this -> descripcion = descripcion;
    this -> precio = 0.0;
    this -> cantProdMenu = 0;
    auto dtM = new DtMenuVendido (this->codigo,this->descripcion, precio, cantProdMenu);
    this->producto = dtM;
    Producto* menu = new Menu(codigo,descripcion,precio);
    this->colProductos[codigo]=menu;
    return DtMenuVendido (codigo,descripcion,precio,cantProdMenu);
}

void CProducto::seleccionarProducto(int idProducto){
    this -> idProducto = idProducto;
}

DtComun CProducto::agregarProducto(int codigo, float precio, string descripcion){
	    this -> codigo = codigo;
    this -> descripcion = descripcion;
    this -> precio = precio;
    auto dtC = new DtComun (this->codigo,this->descripcion, precio);
    this->producto = dtC;
    return DtComun (codigo,descripcion,precio);
}

void CProducto::seleccionarProducto2(int idProducto, int cantidad){
    this -> idProducto = idProducto;
    this -> cantidad = cantidad;
}

void CProducto::crearProducto (DtProducto* producto){
    int id = producto->getIdProducto();
    string des = producto->getDescripcion();
    float pre = producto->getPrecio();

    DtComun* c = dynamic_cast<DtComun*>(producto);
	DtMenuVendido* m = dynamic_cast<DtMenuVendido*>(producto);

	if (c){
		Producto* comun = new Comun(id,des,pre);//dtcomun->getIdProducto(),dtcomun->getDescripcion(),dtcomun->getPrecio());
		colProductos[id]=comun;

	}
	else if (m){
		Producto* menu = new Menu (m->getIdProducto(),m->getDescripcion(),m->getPrecio());
		colProductos[m->getIdProducto()]=menu;
	}
}

void CProducto::agregarProductoMenu(int idMenu, int idProducto, int cantidad) {

	map<int,Producto*> aux = this->colProductos;

	for (map<int, Producto *>::iterator it = aux.begin(); it != aux.end(); ++it) {
        if (it->second->getIdProducto() == idMenu) {
        	Producto* p = it->second;

            Comun* c = dynamic_cast<Comun*>(p);
            Menu* m = dynamic_cast<Menu*>(p);

            if (m){
            	m->agregarP(idProducto,cantidad);
            }
        }
    }
}

float CProducto::obtenerPrecio (int idProducto) {
	for (map<int, Producto *>::iterator it = colProductos.begin(); it != colProductos.end(); ++it) {
		if (it->second->getIdProducto() == idProducto) {
			//it->second->agregarP(idProducto, cantidad);
			Producto *p = it->second;
			return it->second->getPrecio();

		}
	}
}

void CProducto::cambiarPrecio (int idProducto, float precio){
	for (map<int, Producto *>::iterator it = colProductos.begin(); it != colProductos.end(); ++it) {
		if (it->second->getIdProducto() == idProducto) {
			//it->second->agregarP(idProducto, cantidad);
			Producto *p = it->second;
			it->second->setPrecio(precio);

		}
	}
}

void CProducto::cancelar(){}

void CProducto::confirmar(){
    this->crearProducto(this->producto);
}

map<int,DtProducto*> CProducto::listarProductos(){
	map<int,DtProducto*> resultado;
	resultado.clear();

	map<int, Producto*> aux = this->colProductos;

	for (map<int,Producto*>::iterator it=aux.begin(); it!= aux.end(); ++it){
		Producto* p = it->second;

		Comun* c = dynamic_cast<Comun*>(p);
		Menu* m = dynamic_cast<Menu*>(p);

		if (c){
			int id = p->getIdProducto();
			string desc = p->getDescripcion();
			float precio = p->getPrecio();
			DtProducto* co = new DtComun(id,desc,precio);
			resultado[id]=co;
		}
		else if(m){
			int idm = m->getIdProducto();//   ->getIdProducto();
			string descm = m->getDescripcion();
			float preciom = m->getPrecio();
			int cantidad = 0;
			DtProducto* me = new DtMenuVendido(idm,descm,preciom,cantidad);
			resultado[idm]=me;
		}
	}
	return resultado;

}

bool CProducto::existeProd(int idProducto){
	int p = idProducto;
    return colProductos.find(p) != colProductos.end();
}

int CProducto::cantProdComunes(){
	/*int cantidad=2;
	return cantidad;*/
	int cant=0;
    map<int,DtProducto*> listaProducto = listarProductos();
    for (map<int, DtProducto*>::iterator it = listaProducto.begin(); it != listaProducto.end(); it++) {
        DtProducto* dato = it->second;

        DtComun* c = dynamic_cast<DtComun*>(dato);
        DtMenuVendido* m = dynamic_cast<DtMenuVendido*>(dato);

        if (c){
            cant = cant +1;
        }
        else if (m){

        }
    }
    return cant;
}

//void CProducto::confirmarBajaProducto(int idProducto){

DtMenu* CProducto::obtenerProductoMenu(int idProducto){

	Producto* p = this->colProductos.find(idProducto)->second;

	Comun* c = dynamic_cast<Comun*>(p);
	Menu* m = dynamic_cast<Menu*>(p);

//	if (c){
//		cout << "el producto que encontre es un menu"
//		DtProducto* resultado = new DtComun(p->getIdProducto(),p->getDescripcion(),p->getPrecio());
//		return resultado;
//	}
	if (m){
		//float cantidad=0.0;
		DtMenu* resultado = new DtMenu(m->getIdProducto(), m->getDescripcion(),m->getPrecio());
		map<int,MenuComun*> aux = m->obtenerMenuProducto();
		resultado->setearMapa(aux);
		return resultado;
	}
}

DtComun* CProducto::obtenerProductoComun(int idProducto){

	Producto* p = this->colProductos.find(idProducto)->second;

	Comun* c = dynamic_cast<Comun*>(p);
	Menu* m = dynamic_cast<Menu*>(p);

	if (c){
		DtComun* resultado = new DtComun(p->getIdProducto(),p->getDescripcion(),p->getPrecio());
		return resultado;
	}
}



CProducto::~CProducto(){}//=default;


//getInstance devuelve el puntero a la instancia de controlador nueva.

