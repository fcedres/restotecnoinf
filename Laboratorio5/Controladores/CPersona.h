//
// Created by Fabio on 3/6/2020.
//

#ifndef LABORATORIO5_CPERSONA_H
#define LABORATORIO5_CPERSONA_H

#include "../Interfaces/ICPersona.h"
#include "../Empleado.h"
#include "../Cliente.h"
#include "../Mozo.h"
#include "../Repartidor.h"
#include "../DataTypes/DtEmpleado.h"
#include "../DataTypes/DtMozo.h"
#include "../DataTypes/DtRepartidor.h"
#include "../DataTypes/DtApartamento.h"
#include "../DataTypes/DtCliente.h"
#include "../Mesa.h"
#include <list>
#include <map>
#include <iomanip>
#include <iostream>
using namespace std;

class CPersona: public ICPersona{
private:
    static CPersona* instancia;
    CPersona();
    Empleado* empleado;
    Cliente* cliente;
    map<int, Empleado*> colEmpleados;
    map<int, Cliente*> colClientes;
public:

    //operaciones del controlador
    static CPersona* getInstancia();
    //CPersona(Empleado* e, Cliente* c);

    // operaciones del sistema
    list<int> ingresarVentasMesa(int idEmpleado);
    void altaEmpleado(DtEmpleado* empleado);
    void cancelar();
    int obtenerTamanioColeccionEmpleado();
    map<int,DtEmpleado*> listarEmpleados();
    void altaCliente(DtCliente* cliente);
    map<int,DtCliente*> listarClientes();
    void asignarMesaAMozo(int idMozo, Mesa* m);
    //Destructor
    ~CPersona();
};

#endif //LABORATORIO5_CPERSONA_H
