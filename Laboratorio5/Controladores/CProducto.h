//
// Created by Fabio on 3/6/2020.
//

#ifndef LABORATORIO5_CPRODUCTO_H
#define LABORATORIO5_CPRODUCTO_H

class Producto;
#include "../Interfaces/ICProducto.h"

#include "../Producto.h"
#include "../DataTypes/DtProducto.h"
#include "../Menu.h"
#include "../DataTypes/DtMenu.h"
#include "../Comun.h"
#include "../DataTypes/DtComun.h"

#include "../DataTypes/DtMenuVendido.h"
#include "../Controladores/CVentaFacturaMesa.h"
#include <map>
#include <string.h>
#include <list>
#include <iostream>
#include <iomanip>
#include "list"
using namespace std;


class CProducto: public ICProducto{
private:
    static CProducto* instancia;
    CProducto();
    //CProducto(CProducto const&){};
    map<int, Producto*> colProductos;
    int codigo;
    string descripcion;
    float precio;
    int cantProdMenu;
    int idProducto;
    int cantidad;
    DtProducto * producto;
public:

    //operaciones del constructor
    static CProducto* getInstancia();

    //operaciones del sistema
    DtMenuVendido crearMenu(int codigo, string descripcion);
    void seleccionarProducto(int idProducto);
    DtComun agregarProducto(int codigo, float precio, string descripcion);
    void seleccionarProducto2(int idProducto, int cantidad);
    void cancelar();
    void confirmar();
    map<int,DtProducto*> listarProductos();
    void crearProducto (DtProducto* producto);
    void agregarProductoMenu(int idMenu, int idProducto, int cantidad);
    bool existeProd(int idProducto);
    int cantProdComunes();
    DtMenu* obtenerProductoMenu(int idProducto);
    DtComun* obtenerProductoComun(int idProducto);
    float obtenerPrecio (int idProducto);
    void cambiarPrecio (int idProducto, float precio);

    //destructor
    virtual ~CProducto();
};

#endif //LABORATORIO5_CPRODUCTO_H
