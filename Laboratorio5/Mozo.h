#ifndef Laboratorio5_Mozo_h
#define Laboratorio5_Mozo_h

#include "Mesa.h"
//#include "DataTypes/DtMesa.h"
#include "Empleado.h"
#include <map>
#include <string>
#include <iostream>
using namespace std;

class Mozo: public Empleado{

private:
	map<int, Mesa*> colMesas;
public:
	//Constructores

	Mozo();
	Mozo(int IdEmpleado, string nombre);

	//Destructor
	~Mozo();

	list<int> obtenerMesasAsignadasSVC();
	void agregarMesa(Mesa* m);
};
#endif
