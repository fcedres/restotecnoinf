/*
 * Empleado.h
 *
 *  Created on: 11/06/2020
 *      Author: andres
 */

#ifndef LABORATORIO5_EMPLEADO_H_
#define LABORATORIO5_EMPLEADO_H_

#include <string>
using namespace std;
#include <list>

class Empleado{
private:
	int idEmpleado;
	string nombre;
public:
	Empleado();
	Empleado(int idEmpleado, string nombre );

	//Seters

    void setIdEmpleado(int idEmpleado);
    void setNombre(string nombre);

    //Geters
    int getIdEmpleado();
    string getNombre();


    //Destructor
    virtual ~Empleado(); // virtual agregado por Eduardo

    //Casos de uso
    //virtual list <int> obtenerMesasAsignadasSVC(); //Iniciar venta en mesa


};



#endif /* LABORATORIO5_EMPLEADO_H_ */
