#ifndef Laboratorio5_Factura_h
#define Laboratorio5_Factura_h

#include "DataTypes/DtProducto.h"
#include "DataTypes/DtFecha.h"
#include <map>
#include <string>
using namespace std;

class Factura{
private:
	int codigoVenta;
	DtFecha fecha;
	map<int, DtProducto*> productos;
    float subTotalVenta;
    float precioTotal;

public:
	//constructor
	Factura();
	Factura(int codigoVenta, DtFecha fecha, float subTotalVenta, float precioTotal);

	//seters
	int getcodigoVenta();
	DtFecha getfecha();
	float getsubTotalVenta();
	float getprecioTotal();

	//geters
	void setcodigoVenta(int codigoVenta);
	void setfecha(DtFecha fecha);
	void setsubTotalVenta(float subTotalVenta);
	void setprecioTotal(float precioTotal);

	//destructor
	~Factura();
};



#endif
