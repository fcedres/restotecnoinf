/*
 * Cliente.cpp
 *
 *  Created on: 11/06/2020
 *      Author: andres
 */

#include "Cliente.h"



//Constructores

Cliente::Cliente(){
    this -> telefono = 0;
    this->nombre = "";
    this->direccion;// = DtDireccion();
}

Cliente::Cliente(int telefono, string nombre, DtDireccion* direccion){
    this-> telefono = telefono;
    this-> nombre = nombre;
    this-> direccion = direccion;


}

    //Seters

    void Cliente::setTelefono(int telefono) {

        this->telefono=telefono;

    }

    void Cliente::setNombre(string nombre) {

        this->nombre=nombre;

    }

   void Cliente::setDireccion(DtDireccion* direccion) {

        this->direccion=direccion;

    }

    //Geters



    int Cliente::getTelefono(){

        return this->telefono;

    }



    string Cliente::getNombre(){

        return this->nombre;

    }


    DtDireccion* Cliente::getDireccion(){

        return this->direccion;

    }




    //Destructor

    Cliente::~Cliente()=default;
