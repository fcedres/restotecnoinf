//
// Created by Fabio on 3/6/2020.
//

#ifndef LABORATORIO5_FCPRODUCTO_H
#define LABORATORIO5_FCPRODUCTO_H

#include "../Controladores/CProducto.h"
#include "../Interfaces/ICProducto.h"


class FCProducto {
private:
    static FCProducto* instancia;
    FCProducto();
public:
    static FCProducto *getInstancia();
    ICProducto *getICProducto();

    // Destructor
    ~FCProducto();



};

#endif //LABORATORIO5_FCPRODUCTO_H
