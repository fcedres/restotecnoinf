//
// Created by Fabio on 3/6/2020.
//

#include "FCVentaFacturaMesa.h"
#include "../Controladores/CVentaFacturaMesa.h"
FCVentaFacturaMesa* FCVentaFacturaMesa::instancia = nullptr;

FCVentaFacturaMesa::FCVentaFacturaMesa() = default;

FCVentaFacturaMesa* FCVentaFacturaMesa::getInstancia() {
    if (instancia == nullptr) {
        instancia = new FCVentaFacturaMesa();
    }

    return instancia;
}

ICVentaFacturaMesa* FCVentaFacturaMesa::getICVentaFacturaMesa(){

    return CVentaFacturaMesa::getInstancia();

}

FCVentaFacturaMesa ::~FCVentaFacturaMesa(){
    instancia = nullptr;
}
