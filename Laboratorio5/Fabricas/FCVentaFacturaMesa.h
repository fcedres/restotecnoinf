//
// Created by Fabio on 3/6/2020.
//

#ifndef LABORATORIO5_FCVENTAFACTURAMESA_H
#define LABORATORIO5_FCVENTAFACTURAMESA_H

#include "../Controladores/CVentaFacturaMesa.h"
#include "../Interfaces/ICVentaFacturaMesa.h"


class FCVentaFacturaMesa {
private:
    static FCVentaFacturaMesa* instancia;

    FCVentaFacturaMesa();

public:
    static FCVentaFacturaMesa *getInstancia();

    ICVentaFacturaMesa *getICVentaFacturaMesa();

    // Destructor
    ~FCVentaFacturaMesa();



};


#endif //LABORATORIO5_FCVENTAFACTURAMESA_H
