//
// Created by Fabio on 3/6/2020.
//
#include "FCProducto.h"
#include "../Controladores/CProducto.h"
FCProducto* FCProducto::instancia = nullptr;

FCProducto::FCProducto() = default;

FCProducto* FCProducto::getInstancia() {
    if (instancia == nullptr) {
        instancia = new FCProducto();
    }

    return instancia;
}

ICProducto* FCProducto::getICProducto(){
    return CProducto::getInstancia();
}

FCProducto::~FCProducto(){}
