//
// Created by Fabio on 3/6/2020.
//

#ifndef LABORATORIO5_FCPERSONA_H
#define LABORATORIO5_FCPERSONA_H

#include "../Interfaces/ICPersona.h"
#include "../Controladores/CPersona.h"


class FCPersona {
private:
    static FCPersona* instancia;
    FCPersona();
public:
    static FCPersona* getInstancia();
    ICPersona* getICPersona();

    // Destructor
    ~FCPersona();
};
#endif //LABORATORIO5_FCPERSONA_H
