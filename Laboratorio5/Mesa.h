//
// Created by Fabio on 8/6/2020.
//

#ifndef LABORATORIO5_MESA_H
#define LABORATORIO5_MESA_H

#include "Local.h"
#include <map>
#include <iostream>
using namespace std;

class Mesa {

private:
    int numero;
    map<int, Local*> colLocal;
    Local* local;

public:
    //Constructores
    Mesa();

//    Mesa(int numero);

    Mesa(int numero, Local* local);

    //Seters
    void setNumero(int numero);
    int setLocal(Local* local);

    //Geters
    int getNumero();


    //Destructor
    ~Mesa();

    bool tieneVentaEnCurso();
    bool atendidaPorMozo(int idEmpleado);

    //bool mesaSinVenta(); // hace lo mismo que tieneVentaEnCurso


};
#endif //LABORATORIO5_MESA_H
