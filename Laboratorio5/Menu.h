//
// Created by Fabio on 9/6/2020.
//
class MenuComun;
#ifndef LABORATORIO5_MENU_H
#define LABORATORIO5_MENU_H

#include "Producto.h"
#include "MenuComun.h"
#include <map>
#include <string>
#include <iostream>
using namespace std;

class Menu: public Producto{

private:
    map<int, MenuComun*> colMenuComun;

public:
    //Constructores

    Menu();
    Menu(int idProducto, string descripcion, float precio);

    //Seters

    //Geters

    map<int, MenuComun*> obtenerMenuProducto();
    void agregarP (int idProducto, int cantidad);
    //Destructor
    virtual ~Menu();
};
#endif //LABORATORIO5_MENU_H
