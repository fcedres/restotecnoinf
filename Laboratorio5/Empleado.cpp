/*
 * Empleado.cpp
 *
 *  Created on: 11/06/2020
 *      Author: Andres
 */

#include "Empleado.h"

//Constructores

    Empleado::Empleado(){
        this->idEmpleado=0;
        this->nombre="";
    }

    Empleado::Empleado(int idEmpleado, string nombre){
        this->idEmpleado = idEmpleado;
        this->nombre = nombre;
    }

    //Seters

    void Empleado::setIdEmpleado(int idEmpleado) {
        this->idEmpleado=idEmpleado;
    }

    void Empleado::setNombre(string nombre) {
        this->nombre=nombre;
    }

    //Geters

    int Empleado::getIdEmpleado(){
        return idEmpleado;
    }

    string Empleado::getNombre(){
        return nombre;
    }

    //Destructor
    Empleado::~Empleado()=default;
