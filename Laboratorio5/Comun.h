/*
 * Comun.h
 *
 *  Created on: 11/06/2020
 *      Author: eduardo
 */

#ifndef COMUN_H_
#define COMUN_H_

#include "Producto.h"

class Comun: public Producto{

private:
	//map<int,Menu*> menu;

public:
	//constructor
	Comun();
	Comun(int idProducto, string descripcion, float precio);

	//geters
	int getIdProducto();
	string getDescripcion();
	float getPrecio();

	//geters
	void setIdProducto(int idProducto);
	void setDescripcion(string descripcion);
	void setPrecio(float precio);


	//destructor
	~Comun();
};



#endif /* COMUN_H_ */
