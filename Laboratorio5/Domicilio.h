#ifndef LABORATORIO5_DOMICILIO_H
#define LABORATORIO5_DOMICILIO_H

#include "Venta.h"
#include "DataTypes/DtCliente.h"

class Domicilio: public Venta{

private:
	DtCliente cliente;

public:
	//constructor
	Domicilio();
	Domicilio(int IdVenta, Factura* factura, DtCliente cliente);
	//geters
	DtCliente getDtCliente();

	//seters
	void setDtCliente(DtCliente cliente);

	//destructor
	~Domicilio();

};
#endif //LABORATORIO5_DOMICILIO_H
