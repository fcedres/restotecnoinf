//
// Created by Fabio on 3/6/2020.
//

#ifndef LABORATORIO5_ICPERSONA_H
#define LABORATORIO5_ICPERSONA_H

#include "../DataTypes/DtEmpleado.h"
#include "../DataTypes/DtCliente.h"
#include "../Mesa.h"
#include <list>
#include <map>
using namespace std;

class ICPersona {
public:
	virtual list<int> ingresarVentasMesa(int idEmpleado) = 0;
	virtual void cancelar() = 0;
	virtual void altaEmpleado(DtEmpleado* empleado)=0;
	virtual int obtenerTamanioColeccionEmpleado()=0;
	virtual map<int,DtEmpleado*> listarEmpleados()=0;
	virtual void altaCliente(DtCliente* cliente)=0;
	virtual map<int,DtCliente*> listarClientes()=0;
	virtual void asignarMesaAMozo(int idMozo, Mesa* m)=0;
};
#endif //LABORATORIO5_ICPERSONA_H

