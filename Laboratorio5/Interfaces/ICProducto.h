//
// Created by Fabio on 3/6/2020.
//

#ifndef LABORATORIO5_ICPRODUCTO_H
#define LABORATORIO5_ICPRODUCTO_H

#include <string>
#include <list>
#include <map>
#include "../DataTypes/DtMenuVendido.h"
#include "../DataTypes/DtComun.h"
#include "../DataTypes/DtProducto.h"
#include "../DataTypes/DtMenu.h"
using namespace std;

class ICProducto {
public:
	virtual DtMenuVendido crearMenu(int codigo, string descripcion)=0;
	virtual void seleccionarProducto(int idProducto)=0;
	virtual DtComun agregarProducto(int codigo, float precio, string descripcion)=0;
	virtual void seleccionarProducto2(int idProducto, int cantidad)=0;
	virtual void cancelar()=0;
	virtual void confirmar()=0;
	virtual map<int,DtProducto*> listarProductos()=0;
    virtual void crearProducto (DtProducto* producto)=0;
    //virtual void agregarProductoMenu(int idProducto, int cantidad)=0;
    virtual void agregarProductoMenu(int idMenu, int idProducto, int cantidad)=0;
    virtual bool existeProd(int idProducto)=0;
    virtual int cantProdComunes()=0;
//    virtual DtProducto* obtenerProducto(int idProducto)=0;
    virtual DtMenu* obtenerProductoMenu(int idProducto)=0;
    virtual DtComun* obtenerProductoComun(int idProducto)=0;
    virtual float obtenerPrecio (int idProducto)=0;
	virtual void cambiarPrecio (int idProducto, float precio)=0;
};
#endif //LABORATORIO5_ICPRODUCTO_H
