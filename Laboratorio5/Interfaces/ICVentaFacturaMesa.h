//
// Created by Fabio on 3/6/2020.
//

#ifndef LABORATORIO5_ICVENTAFACTURAMESA_H
#define LABORATORIO5_ICVENTAFACTURAMESA_H

#include <list>
#include "../DataTypes/DtFactura.h"
#include "../DataTypes/DtCliente.h"
#include "../Mesa.h"
#include <iostream>
#include <map>
#include <iomanip>
using namespace std;

class ICVentaFacturaMesa {
public:
	//	virtual void disminuirCantidad()=0;
//	virtual int obtenerCantidadProductos()=0;
//	virtual list<int> listarVentasNoFacturadas()=0;
//	virtual void incrementarCantidad()=0;
//	virtual void seleccionarVenta(int idVenta)=0;
//	//virtual list<int> ingresarVentaMesa(int idEmpleado)=0; //no va
//	virtual list<int> listarMesas()=0;
//	virtual void quitarProducto()=0;
//	virtual list<int> listarProductosDeLaVenta()=0;
//	virtual void seleccioarMesa(int numero)=0;
//	//virtual void confirmarBaja()=0;
//	virtual void agregarProductoMesa()=0;
//	virtual DtFactura ventaMesa(int idMesa, float descuento)=0;
////	//virtual list<int> iniciarVenta(int idEmpleado)=0; // se corrigio el diagrama de comunicacion
	virtual int iniciarVenta(list<int> numemro)=0;
	virtual int iniciarVentaDomicilio(DtCliente cliente)=0;
	virtual bool confirmarBajaProducto(int idProducto)=0;
	virtual void crearMesa(int numero)=0;
	virtual map<int,Mesa*> obtenerMesas()=0;
	virtual void asignarMozoAMesa()=0;
//	virtual list<int> obtenerVentas()=0;
};
#endif //LABORATORIO5_ICVENTAFACTURAMESA_H
