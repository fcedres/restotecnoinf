//
// Created by Fabio on 3/6/2020.
//

#include <set>
#include "MenuPrin.h"

#define clearscr() printf("\033[H\033[J");
using namespace std;

// Instancia del Menu Principal.
MenuPrin* MenuPrin::instancia = nullptr;

// Constructor
MenuPrin::MenuPrin() {
    // Instanciando las Fabricas.
    this->FPC = FCProducto::getInstancia();
    this->FVFMC = FCVentaFacturaMesa::getInstancia();
    this->FPerC = FCPersona::getInstancia();

    //Instanciando las Interfaces.
    this->IPC = this->FPC->getICProducto();
    this->IVFMC = this->FVFMC->getICVentaFacturaMesa();
    this->IPerC = this->FPerC->getICPersona();
}

MenuPrin* MenuPrin::getInstancia() {

    if (instancia == nullptr) {
        instancia = new MenuPrin();
    }

    return instancia;
}

void MenuPrin::optSalir(){
//S- SALIR


}

//////////////////////////////////////IMPRESION Y MANEJO DE MENUES//////////////////////////////////////////////////////


void MenuPrin::impMenuAdmin(){
    clearscr();
    bool salirMA = false;
    do{
		int opA;
		cout << endl << "  Bienvenido a Restotecnoinf!" << endl << endl;
		cout << " \n  Menú ADMINISTRATIVO\n " << endl;
		cout << "    0- Alta Producto	¡¡¡ PRONTA!!!" << endl;
		cout << "    1- Alta Cliente	¡¡¡PRONTA!!!" << endl;
		cout << "    2- Alta Empleado	¡¡¡PRONTA!!!" << endl;
		cout << "    3- Asignar automáticamente mozos a mesas	¡¡¡PRONTA!!!" <<endl;
		cout << "    4- Venta a domicilio	FALTA" <<endl;
		cout << "    5- Ventas de un mozo	FALTA" << endl;
		cout << "    6- Informacion de un producto	¡¡¡PRONTA!!!" << endl;
		cout << "    7- Resumen facturación de un día dada la fecha	FALTA" << endl;
		cout << "    8- Baja Producto	FALTA" << endl;
		cout << endl;
		cout << "    9- << VOLVER" << endl;

		cout << endl << "  Elija una opcion: ";
		cin >> opA;
		if (opA == 0){ //Alta Producto
			int id, opP, ps, cant, opC;
			bool seguir = false;
			string descripcion;
			float precio;
			float sum=0.0, pre = 0.0;
			cout << "Ingrese el código: ";
			cin  >> id;
            if (this->IPC->existeProd(id)){
            	cout << "El producto ingresado ya existe" << endl;
            }
            else if ((this->IPC->cantProdComunes()) > 1){
                cout << "Desea agregar un: "<<endl;
                cout<< "    0- << Menú" << endl;
                cout<< "    1- << Comun" << endl;
                cout << endl << "  Elija una opcion: ";
                cin>> opP;
                do{
                    if (opP==0){
                        cout << "Ingrese la descripcion del menu: ";
                        cin >> descripcion;
                        this->IPC->crearMenu(id,descripcion);
                        cout << "Productos disponibles: " << endl;
                        map<int,DtProducto*> listaProducto = IPC-> listarProductos();
                        for (map<int, DtProducto*>::iterator it = listaProducto.begin(); it != listaProducto.end(); it++) {
                            DtProducto* dato = it->second;

                            DtComun* c = dynamic_cast<DtComun*>(dato);
                            DtMenuVendido* m = dynamic_cast<DtMenuVendido*>(dato);

                            if (c){
								cout << it->second->getIdProducto() << " " << it->second->getDescripcion() <<
										 " " << it->second->getPrecio()<<endl;
								cout << "-----------------------------------"<<endl;
                            }
                            else if (m){
                            }
                        }
                        do {
							cout << "Elija un producto: ";
							cin >> ps;
							cout << "Cuantos desea agregar: ";
							cin >> cant;
							//this->IPC->seleccionarProducto2(ps, cant);
							//this->IPC->agregarProductoMenu(id,cant);
							pre = this->IPC->obtenerPrecio(ps);
							pre = pre * cant;
							cout << sum << endl;
							sum = sum + pre;
							this->IPC->agregarProductoMenu(id, ps, cant);
							cout << "Desea seguir agregando productos al menu?: " <<endl;
							cout<< "    0- << Si" << endl;
							cout<< "    1- << No" << endl;
							cout << endl << "  Elija una opcion: ";
							cin >>opC;
							if (opC==0){
								seguir = true;
							}else {
								seguir = false;
							}
						}while(seguir);
                        sum = sum -((sum *10)/100);
						this ->IPC->cambiarPrecio(id,sum);
						cout << "El precio del menu ingresado es: " << sum <<endl;
                    }
                    if (opP==1){
                        cout << "Ingrese la descripcion del producto: ";
                        cin >> descripcion;
                        cout << "Ingrese el precio del producto: ";
                        cin >> precio;
                        DtProducto* dtProducto = new DtComun (id,descripcion,precio);
                        this->IPC->crearProducto(dtProducto);
                    }
                }while(opP > 1);
            }
            else{
                cout << "Ingrese la descripcion del producto: ";
                cin >> descripcion;
                cout << "Ingrese el precio del producto: ";
                cin >> precio;
                DtProducto* dtProducto = new DtComun (id,descripcion,precio);
                this->IPC->crearProducto(dtProducto);
            }

		}
		if (opA ==1){//Alta Cliente
			string nombre,calle,esq1,esq2,edi;
			int telefono,nroPuerta,apto,nApto;

			cout << "ingrese el nombre: ";
			cin >> nombre;
			cin.clear();
			cout << "ingrese el telefono: ";
			cin >> telefono;
			cout << "Direccion" << endl;
			cout << "calle: ";
			cin >> calle;
			cin.clear();
			cout << "nro puerta: ";
			cin >> nroPuerta;
			cout << "esquina 1: ";
			cin >> esq1;
			cin.clear();
			cout << "esquina 2: ";
			cin >> esq2;
			cin.clear();

			cout << "¿es apartamento?" << endl;
			cout << "0-si" << endl;
			cout << "1-no" << endl;
			cin >> apto;
			do{
				if (apto==0){
					cout << "ingrese el nombre del edificio: ";
					cin >> edi;
					cin.clear();
					cout << "Nro de Apto: ";
					cin >>nApto;
					DtDireccion* dir1 = new DtApartamento(calle,nroPuerta,esq1,esq2,edi,nApto);
					DtCliente* cliente1 = new DtCliente(telefono,nombre,dir1);
					IPerC->altaCliente(cliente1);
				}
				if (apto==1){
					DtDireccion* dir = new DtDireccion(calle,nroPuerta,esq1,esq2);
					DtCliente* cliente = new DtCliente(telefono,nombre,dir);
					IPerC->altaCliente(cliente);
				}
			}while(apto > 1);
			this->impMenuAdmin();
		}

		if (opA == 2){//Alta empleado

			int a = IPerC->obtenerTamanioColeccionEmpleado();
			int idEmpleado = a+1;
			cout << "idEmpleado = " << idEmpleado << endl;

			int op;
			string nombre;

			do{
				cout << "0-Crear Mozo" << endl;
				cout << "1-Crear Repartidor" << endl;
				cout << endl;
				cout << "2- <-- Volver" << endl;
				cin >> op;

				if (op==0){ //creacion DtMozo
					cout << "ingrese el nombre: ";
					cin >> nombre;
					DtEmpleado* empleado = new DtMozo(idEmpleado, nombre);
					IPerC->altaEmpleado(empleado);
					this->impMenuAdmin();
				}

				if(op==1){ //creacion DtTransporte
					tipoTransporte transporte;
					int tra=0;
					cout << "ingrese el nombre: ";
					cin >> nombre;

					do{
						cout << "ingrese el medio de transporte: " << endl;
						cout << "	0-pie" << endl;
						cout << "	1-bici" << endl;
						cout << "	2-moto" << endl;
						cin >> tra;
						if (tra==0){
							transporte=pie;
						}
						else if(tra==1){
							transporte=bici;
						}
						else if(tra==2) {
							transporte=moto;
						}
						else{
							cout << "opcion invalida" << endl;
						}
					}while(tra>2);

					DtEmpleado* empleado = new DtRepartidor(idEmpleado, nombre, transporte);
					IPerC->altaEmpleado(empleado);
				}
				this->impMenuAdmin();
				if (op==2){
					this->impMenuAdmin();
				}
			}while(op!=2);
		}

		if (opA ==3){//Asignar automáticamente mozos a mesas
			this->MenuPrin::menuAsignarMozoAMesa();
        }
		if (opA ==4){//Venta a Domicilio
					this->MenuPrin::menuVentaDomicilio();
		}
		if (opA == 6){
			this->MenuPrin::MenuInformacionDeProductos();
		}
		if (opA == 8){
			this->MenuPrin::menuBajaProducto();
		}

		if (opA == 9){
			this-> MenuPrin::mMenuPrin();
		}
    }while(!salirMA);

}

void MenuPrin::impMenuCliente() {
    clearscr();

    bool salirMC = false;
    do{
    	int opC;
		cout << endl << "  Bienvenido a Restotecnoinf!" << endl << endl;
		cout << " \n  Menú Cliente\n " << endl;
		cout << "    1- << VOLVER" << endl;

		cout << endl << "  Elija una opcion: ";
		cin >> opC;
		if (opC==1){
			this-> MenuPrin::mMenuPrin();
		}
    }while(!salirMC);
}

void MenuPrin::impMenuMozo() {
    clearscr();

    bool salirMM = false;
    do{
    	int opM;
		cout << endl << "  Bienvenido a Restotecnoinf!" << endl << endl;
		cout << " \n  Menú Mozo\n " << endl;
		cout << "    0- Iniciar venta en mesas	¡¡¡PRONTA!!!5" << endl;
		cout << "    1- Agregar producto a una venta	FALTA" << endl;
		cout << "    2- Quitar producto a una venta	FALTA" << endl;
		cout << "    3- Facturación de una venta	FALTA" << endl;
		cout << endl;
		cout << "    4- << VOLVER" << endl;

		cout << endl << "  Elija una opcion: ";
		cin >> opM;
		if (opM==0){

			clearscr();
			int idMozo;
			list<int>mesasDelMozo;
			list<int>mesasElegidasPorMozo;

			cout << "ingrese su Id: ";
			cin >> idMozo;


			mesasDelMozo= IPerC->ingresarVentasMesa(idMozo);

			if (mesasDelMozo.size()!=0){
				cout << "Las mesas que tiene el mozo asignadas sin ventas son las siguientes: " << endl;
				int opcion;

				for(list<int>::iterator it=mesasDelMozo.begin(); it!=mesasDelMozo.end(); it++){
					cout << "mesa nro "<< *it << endl;
					cout << "Desea elegir esta mesa para la venta s/n" << endl;
					cout << "0-no" << endl;
					cout << "1-si" << endl;
					cin >> opcion;
					if (opcion == 1){
						mesasElegidasPorMozo.push_front(*it);
					}
				}
				if (mesasElegidasPorMozo.size()==0){
					cout << "No se creo la venta porque no se eligieron mesas" << endl;
				}
				else{
					int idVenta = IVFMC->iniciarVenta(mesasElegidasPorMozo);
					cout << "se creo la venta con el id " << idVenta << " que comprende a la/las mesa/s " << endl;
					for(list<int>::iterator it=mesasElegidasPorMozo.begin(); it!= mesasElegidasPorMozo.end(); ++it){
						cout << *it << endl;
					}
				}
			}
			else{
				cout << "El Mozo ingesado no tiene mesas que cumplan con esa condicion" << endl;
			}
		}// fin de opcion 0

		if (opM == 4){
			this-> MenuPrin::mMenuPrin();
		}
    }while(!salirMM);
}

void MenuPrin::impMenuRepartidor() {
    clearscr();

    bool salirMR = false;
    do{
    	int opR;
		cout << endl << "  Bienvenido a Restotecnoinf!" << endl << endl;
		cout << " \n  Menú Repartidor\n " << endl;
		cout << "    1- << VOLVER" << endl;
		cout << endl << "  Elija una opcion: ";
		cin >> opR;
		if (opR == 1){
			this-> MenuPrin::mMenuPrin();
		}
    }while(!salirMR);

}

void MenuPrin::menuBajaProducto(){
	map<int,DtProducto*> listaProducto = IPC-> listarProductos();
	int idProElegido=0;
	if (listaProducto.size()==0){
		cout << "No existen productos en el sistema" << endl;
	}
	else{
		for(map<int,DtProducto*>::iterator it=listaProducto.begin(); it!=listaProducto.end(); ++it){
			int id = it->second->getIdProducto();
			string desc = it->second->getDescripcion();
			float precio = it->second->getPrecio();
			cout << "***Listado de productos***" << endl;
			cout << endl;
			cout << id << " " << desc << " $" << precio << endl;
			cout << "--------------------------------------------------------------------" << endl;
		}//fin del for
	}//fin del if
	cin >> idProElegido;

	bool eliminarProductoSistema = IVFMC->confirmarBajaProducto(idProElegido);
	if (eliminarProductoSistema){
		//IPC->eliminarProductoSistema(idProdElegido);
	}

}

void MenuPrin::cargarDatosDePrueba() {
    clearscr();
    cout << endl;

    string nom1,nom2,nom3,nombre1,nombre2,nombre3;
    nombre1="Tito"; nombre2="Raul"; nombre3="Tiki_Gelena";
    nom1="Juan_Pedro"; nom2="Johnny"; nom3="Jose";
    tipoTransporte transporte1,transporte2,transporte3;
    transporte1=bici; transporte2=moto; transporte3=pie;

    //Creación de empleados
    DtEmpleado* empleado4 = new DtMozo(1,nom1);
    DtEmpleado* empleado5 = new DtMozo(2,nom2);
    DtEmpleado* empleado6 = new DtMozo(3,nom3);
    IPerC->altaEmpleado(empleado4); IPerC->altaEmpleado(empleado5); IPerC->altaEmpleado(empleado6);

    DtEmpleado* empleado1 = new DtRepartidor(4, nombre1, transporte1);
    DtEmpleado* empleado2 = new DtRepartidor(5, nombre2, transporte2);
    DtEmpleado* empleado3 = new DtRepartidor(6, nombre3, transporte3);
    IPerC->altaEmpleado(empleado1); IPerC->altaEmpleado(empleado2); IPerC->altaEmpleado(empleado3);

    //Creación de Clientes
   // DtDireccion* dir = new DtApartamento(calle,nroPuerta,esq1,esq2,edi,nApto);

    DtDireccion* dir1 = new DtDireccion("Cabildo",500,"esq1","esq2");
    DtCliente* cliente1 = new DtCliente(91651259,"Martin",dir1);
    IPerC->altaCliente(cliente1);

    DtDireccion* dir2 = new DtApartamento("18DeJulio",2033,"esq1","esq2","MarAustral",701);
    DtCliente* cliente2 = new DtCliente(98217523,"Vladimir",dir2);
    IPerC->altaCliente(cliente2);

    DtDireccion* dir3 = new DtApartamento("AvenidaBrasil",842,"esq1","esq2","Atlantis",302);
    DtCliente* cliente3 = new DtCliente(97321590,"Ronaldo",dir3);
    IPerC->altaCliente(cliente3);



    //Creacion de Productos comunes
    DtProducto* dtProducto1 = new DtComun (1,"PizzaMuzarella",130);DtProducto* dtProducto2 = new DtComun (2,"Milanesac/Guarnicion",230);DtProducto* dtProducto3 = new DtComun (3,"CocaCola_1.25L",95);DtProducto* dtProducto4 = new DtComun (4,"EmpanadaCapresse",72);DtProducto* dtProducto5 = new DtComun (5,"Flanc/DulceDeLecha",110);
    this->IPC->crearProducto(dtProducto1); this->IPC->crearProducto(dtProducto2);this->IPC->crearProducto(dtProducto3);this->IPC->crearProducto(dtProducto4);this->IPC->crearProducto(dtProducto5);

    //Creacion de menus
    this->IPC->crearMenu(6,"ComboPizza");this->IPC->crearMenu(7,"ComboEmpanadas");
    //Agregar productos a menu
    int precio1,precio3,precio4,precio6,precio7;
    precio1=IPC->obtenerProductoComun(1)->getPrecio();
    precio3=IPC->obtenerProductoComun(3)->getPrecio();
    precio4=IPC->obtenerProductoComun(4)->getPrecio();
    IPC->agregarProductoMenu(6,1,1);IPC->agregarProductoMenu(6,3,1);
    IPC->agregarProductoMenu(7,3,1);IPC->agregarProductoMenu(7,4,3);
    precio6=precio1+precio3;
    precio6=precio6 -((precio6 *10)/100);
    precio7=precio3+(3*precio4);
    precio7=precio7 -((precio7 *10)/100);
    IPC->cambiarPrecio(6,precio6);
    IPC->cambiarPrecio(7,precio7);

    //crear Mesas

    IVFMC->crearMesa(1);
    IVFMC->crearMesa(2);
    IVFMC->crearMesa(3);
    IVFMC->crearMesa(4);
    IVFMC->crearMesa(5);
    IVFMC->crearMesa(6);
    IVFMC->crearMesa(7);

    cout << "LOS DATOS SE CARGARON DE FORMA EXITOSA" << endl;

}

void MenuPrin::MenuListarEmpleados(){
	cout << endl;
	map<int,DtEmpleado*> listas = IPerC->listarEmpleados();
	if (listas.size()==0){
			cout << "No existen empleados ingresado en el sistema" << endl;
		}
	else{
		cout << "Listado de empleados:" << endl;
		for (map<int,DtEmpleado*>::iterator lista=listas.begin();
				lista!=listas.end(); ++lista){

			DtMozo* mozo =  dynamic_cast<DtMozo*>(lista->second);
			DtRepartidor* repartidor = dynamic_cast<DtRepartidor*>(lista->second);

			if (mozo){
				cout << "mozo--> " << " nombre:" << mozo->getNombre() << " id:" << mozo->getIdEmpleado() << endl;
				cout << "--------------------------------------------------------------------" << endl;
			}
			else if (repartidor){
				string transporte;
				if (repartidor->getTransporte()==0){
					transporte = "pie";
				}
				else if (repartidor->getTransporte()==1){
					transporte = "bici";
				}
				else if (repartidor->getTransporte()==2){
					transporte = "moto";
				}
				cout << "repartidor--> " << " nombre:" << repartidor->getNombre() << " id:" << repartidor->getIdEmpleado() << " transporte:" << transporte << endl;
				cout << "--------------------------------------------------------------------" << endl;
			}
		}//fin del for
	}
}
//para comitear


void MenuPrin::MenuListarClientes(){
	cout << endl;
	map<int,DtCliente*> listas = IPerC->listarClientes();
	if (listas.size()==0){
		cout << "No existen clientes ingresado en el sistema" << endl;
	}
	else{
		cout << endl;
		cout << "**** Listado de clientes ****" << endl;
		cout << "----------------------------------------------------" << endl;
		for (map<int,DtCliente*>::iterator lista=listas.begin();
				lista!=listas.end(); ++lista){

			DtDireccion* dire =  dynamic_cast<DtDireccion*>(lista->second->getDireccion());//  second);
			DtApartamento* apar = dynamic_cast<DtApartamento*>(lista->second->getDireccion());

			if (apar){

				string calle = apar->getNombreCalle();
				string edi = apar->getNombreEdificio();
				string e1 = lista->second->getDireccion()->getEsquina1();
				string e2 = lista->second->getDireccion()->getEsquina2();
				int puerta = lista->second->getDireccion()->getNumeroPuerta();
				int ap = apar->getNroApto();

				cout << "El cliente se llama: " << lista->second->getNombre() << " su telefono es: " << lista->second->getTelefono() << endl;
				cout << "y vive en : " << calle << " " << puerta << " entre " << e1 << " y " << e2 << " en el edificio llamado " << edi << " en el apartamento " << ap << endl;
				cout << "-----------------------------------------------------------------------------------------------------------------------------------------------" << endl;

			}
			else if (dire){
				cout << "El cliente se llama: " << lista->second->getNombre() << " su telefono es: " << lista->second->getTelefono() << " y vive en : " << endl;
				cout << dire->getNombreCalle() << " " << dire->getNumeroPuerta() << " entre " << dire->getEsquina1() << " y " << dire->getEsquina2() << endl;
				cout << "-----------------------------------------------------------------------------------------------------------------------------------------------"<< endl;
			}
		}//fin del for
	}
}

void MenuPrin::MenuListarProductos(){

	map<int,DtProducto*> listaProducto;
	listaProducto.clear();
	listaProducto= IPC-> listarProductos();

		if (listaProducto.size()==0){
			cout << "No existen productos en el sistema" << endl;
		}
		else{
			cout << "***Listado de productos***" << endl;
			for(map<int,DtProducto*>::iterator it=listaProducto.begin(); it!=listaProducto.end(); ++it){
				int id = it->second->getIdProducto();
				string desc = it->second->getDescripcion();
				float precio = it->second->getPrecio();
				cout << endl;
				cout << id << " " << desc << " $" << precio << endl;
				cout << "--------------------------------------------------------------------" << endl;
			}//fin del for
		}
}

void MenuPrin::MenuListarRepartidores(){
	cout << endl;
		map<int,DtEmpleado*> listas = IPerC->listarEmpleados();
		if (listas.size()==0){
				cout << "No existen empleados ingresado en el sistema" << endl;
		}
		else{
			cout << "Listado de repartidores:" << endl;
			for (map<int,DtEmpleado*>::iterator lista=listas.begin();
					lista!=listas.end(); ++lista){

				DtRepartidor* repartidor = dynamic_cast<DtRepartidor*>(lista->second);

				if (repartidor){
					string transporte;
					if (repartidor->getTransporte()==0){
						transporte = "pie";
					}
					else if (repartidor->getTransporte()==1){
						transporte = "bici";
					}
					else if (repartidor->getTransporte()==2){
						transporte = "moto";
					}
					cout << "id: " << repartidor->getIdEmpleado() << " nombre: " << repartidor->getNombre() << " transporte: " << transporte << endl;
					cout << "------------------------------------------" << endl;
				}
			}//fin del for
		}
}

void MenuPrin::MenuInformacionDeProductos(){
	int idProd;
	this->MenuPrin::MenuListarProductos();
	cout << endl;
	cout << "Ingrese el nro de Producto para ver su información: ";
	cin >> idProd;

	DtComun* c = dynamic_cast<DtComun*>(IPC->obtenerProductoComun(idProd));

	if(c){
		cout << "descripción: " << c->getDescripcion() << endl;
	}

	DtMenu* m = dynamic_cast<DtMenu*>(IPC->obtenerProductoMenu(idProd));

	if (m){
		map<int,MenuComun*> aux = m->obtenerMenuProducto();
		cout << "cantidad de elementos de menu son: " << m->obtenerMenuProducto().size() << endl;
		cout << "El menu ** " << m->getDescripcion() << " ** esta compuesto por los siguientes prductos: " << endl;

		for (map<int,MenuComun*>::iterator it=aux.begin(); it!= aux.end(); ++it){
			int idProd = it->first;
			DtComun* p = IPC->obtenerProductoComun(idProd);
			string descripcion = p->getDescripcion();
			cout << it->second->getCantProdMenu() << " unidades de : " << descripcion << endl;// second->obtenerNombreProducto();
		}
	}
}

void MenuPrin::menuListarMesas(){
	map<int,Mesa*> aux = IVFMC->obtenerMesas();
	if (aux.size()==0){
		cout << "no hay mesas ingresadas en el sistema" << endl;
	}
	else{
		cout << "Los numeros de mesas existentes son los siguientes: " << endl;
		for(map<int,Mesa*>::iterator it = aux.begin(); it != aux.end(); ++it){
			cout << it->second->getNumero() << endl;
		}
	}
}

void MenuPrin::menuAsignarMozoAMesa(){
	int num,c;
	int mesaXmozo=0;
	srand(time(NULL));

	int mesas = this->IVFMC->obtenerMesas().size();


	map<int,DtEmpleado*> listas = IPerC->listarEmpleados();
	int mozos=0;

	// obtener cantidad de mozos
	for (map<int,DtEmpleado*>::iterator lista=listas.begin();
			lista!=listas.end(); ++lista){
			DtMozo* mozo =  dynamic_cast<DtMozo*>(lista->second);

			if (mozo){
			mozos = mozos +1;
			}
	}
	mesaXmozo=(mesas/mozos);
	int resto = (mesas%mozos);
	cout << "mesas por mosos = " << mesaXmozo << endl;


	//asignar mozo a mesa
	list<int> lis;
	int ini =1;
	list<int> noAsignadas;

	for (int i = ini; i<=mesas; i++){ //carga inicial de los numeros de mesa
		noAsignadas.push_front(i);
	}

	for (map<int,DtEmpleado*>::iterator lista=listas.begin(); lista!=listas.end(); ++lista){
		DtMozo* mozo =  dynamic_cast<DtMozo*>(lista->second);
		if (mozo){
			int contador = mesaXmozo;
			int aux = mesaXmozo;

			do{
				for(c = 1; c <= aux; c++){
					num = 1 + rand() % (mesas - 1); //se genera el nro de mesa de forma aleatoria
					bool existe=false;
						for(list<int>::iterator it = lis.begin(); it != lis.end(); ++it){ //recorro la lista de mesas
							//aux para buscar si existe en ella el Nro generado aleatoriamente
							if (num==*it){
								existe = true;
							}
						}
						if(!existe){
							lis.push_front(num); // si el numero generado no existe, lo agrego a la lista
							noAsignadas.remove(num);// quito el numero de mesa de la lista para asignarlo luego
							contador = contador -1;
							Mesa* mesa = this->IVFMC->obtenerMesas().find(num)->second;
							IPerC->asignarMesaAMozo(mozo->getIdEmpleado(),mesa);
							cout << "al mozo " << mozo->getIdEmpleado() << " se le asigno la mesa: " << num << endl;
						}
				}
			}while(contador!=0);
		}
	}
	cout << endl;
	cout << "Quedan " << resto << " mesas para asignar" << endl;
	cout << endl;

	//asignar el resto de las mesas
	do{
		int idMozo,primerElemento,numMozo;
		int existe=0;
		do{
			numMozo = 1 + rand() % (40- 1); //generar numero de mozo aleatoriamente

			map<int,DtEmpleado*> list = IPerC->listarEmpleados();
			for (map<int,DtEmpleado*>::iterator l=list.begin(); l!=list.end(); l++){

				DtMozo* mozo =  dynamic_cast<DtMozo*>(l->second);
				if(mozo){
					if (numMozo == mozo->getIdEmpleado()){
						existe = 1;
						idMozo = mozo->getIdEmpleado();
					}
				}
			} // fin del for
		}while(existe!=1);

		if(existe==1){
			primerElemento = noAsignadas.front(); // elijo la primer mesa no asignada
			Mesa* mesa = this->IVFMC->obtenerMesas().find(primerElemento)->second;
			IPerC->asignarMesaAMozo(idMozo,mesa);
			noAsignadas.remove(primerElemento);
			cout << "al mozo " << idMozo << " se le asigno la mesa: " << primerElemento << endl;
		}
	}while(noAsignadas.size()!=0);
}

void MenuPrin::menuVentaDomicilio(){
	map<int,DtCliente*> aux = IPerC->listarClientes();
	int telCli,nroPuerta,apto,apart,entregarPedido,idRepartidor;
	string nombre,calle,esq1,esq2,nomEdi;

	cout << "ingrese el Nro de telefono del cliente: ";
	cin >> telCli;

	if (aux.find(telCli) == aux.end() ){
		cout << "El cliente no existe" << endl;
		cout << endl;
		cout << "ingrese el nombre: ";
		cin >> nombre;
		cin.clear();
		cout << "calle: ";
		cin >> calle;
		cin.clear();
		cout << "nro puerta: ";
		cin >> nroPuerta;
		cout << "esquina 1: ";
		cin >> esq1;
		cin.clear();
		cout << "esquina 2: ";
		cin >> esq2;
		cin.clear();

		cout << "¿es apartamento?" << endl;
		cout << "0-si" << endl;
		cout << "1-no" << endl;
		cin >> apto;

		if (apto == 0){ // se crea el cliente si no existe
			cout << "Ingrese el Nro de apartamento: ";
			cin >> apart;
			cout << "Ingrese el nombre del Edificio: ";
			cin >> nomEdi;
			DtDireccion* dir1 = new DtApartamento(calle,nroPuerta,esq1,esq2,nomEdi,apart);
			DtCliente* cliente1 = new DtCliente(telCli,nombre,dir1);
			IPerC->altaCliente(cliente1);
		}
		if (apto==1){
			DtDireccion* dir = new DtDireccion(calle,nroPuerta,esq1,esq2);
			DtCliente* cliente = new DtCliente(telCli,nombre,dir);
			IPerC->altaCliente(cliente);
		}
	}
	else{
		cout << "El cliente existe" << endl;
	}

	//mostrar productos

	int seguir=0,idProd,cantProd;
	string descripcion;
	float precio;
	list<DtProductoVendido*> productos;
	//list<int> productos;

	do{
	MenuListarProductos();

	//seleccionar productos
	cout << "selecciones el id del pruducto: ";
	cin >> idProd;
	cout << "Seleccione la cantidad: ";
	cin >> cantProd;
	DtComun* producto = IPC->obtenerProductoComun(idProd);
	descripcion = producto->getDescripcion();
	precio = producto->getPrecio();

	DtProductoVendido* nuevo = new DtProductoVendido(idProd,precio,descripcion,cantProd);

	productos.push_front(nuevo);

	cout << "¿Desea agregar mas productos?" << endl;
	cout << "0- si" << endl;
	cout << "1- no" << endl;
	cin >> seguir;

	}while(seguir==0);

	// enviar pedido
	cout << "El pedido debe ser entregado" << endl;
	cout << "0- si" << endl;
	cout << "1- no" << endl;
	cin >> entregarPedido;

	//Mostrar y seleccionar repartidores
	if (entregarPedido==0){
		cout << endl;
		MenuListarRepartidores();
		cout << endl;
		cout << "Ingrese el Nro del repartidor: ";
		cin >> idRepartidor;
	}
//	int nroVenta = IVFMC->iniciarVentaDomicilio();
//	cout << "El numero de la venta es: " << nroVenta;
	//Generar la factura con el id de la venta
	//Agregar dicha factura a la venta cuyo id es nroVenta
	//Cargar la list<VentaProducto*> colVentaProducto con los datos de la list<DtProductoVendido*> productos; y la cantidad
	//correspondiente a cada producto.5

}
void MenuPrin::mMenuPrin() {

    bool salir=false;
    do{
        int opt;
        cout << endl << " Bienvenido a Restotecnoinf!" << endl << endl;
        cout << "    1- Administrador" << endl;
        cout << "    2- Mozo" << endl;
        cout << "    3- Repartidor" << endl;
        cout << "    4- Cliente" << endl;
        cout << "    5- Cargar datos de pruebas	**faltaron las ventas a domicilio**" << endl;
        cout << "    6- Listar Empleados" << endl;
        cout << "    7- Listar Clientes" << endl;
        cout << "    8- Listar Productos" << endl;
        cout << "    9- Listar Mesas" << endl;
        cout << "    10- Salir" << endl;
        cout << endl << "  Elija una opcion: ";

        cin >> opt;
        cin.ignore();
        try {
            if (opt == 1) {
                cout << "Opcion 1" << endl;
               this-> MenuPrin::impMenuAdmin();
            }
            else if (opt == 2) {
                cout << "Opcion 2" << endl;
                this->MenuPrin::impMenuMozo();
            }
            else if (opt == 3) {
                cout << "Opcion 3" << endl;
                this->MenuPrin::impMenuRepartidor();
            }
            else if (opt == 4) {
                cout << "Opcion 4" << endl;
                this->MenuPrin::impMenuCliente();
            }
            else if (opt == 5) {
                cout << "Opcion 5" << endl;
                this->MenuPrin::cargarDatosDePrueba();
            }
            else if (opt == 6) {
            	cout << "Opcion 6" << endl;
            	this->MenuPrin::MenuListarEmpleados();
            	//this->MenuPrin::mMenuPrin();
            }
            else if (opt == 7) {
				cout << "Opcion 7" << endl;
				this->MenuPrin::MenuListarClientes();
			}
            else if (opt == 8) {
            	cout << "Opcion 8" << endl;
            	this->MenuPrin::MenuListarProductos();
            }
            else if (opt == 9) {
                cout << "Opcion 9" << endl;
                this->MenuPrin::menuListarMesas();
            }
            else if (opt == 10) {
                cout << "SALIR" << endl;
                exit(-1);
            }
        }
        catch (invalid_argument &e) {
            cout << "Opcion no válida!" << endl;
        }
        this->optSalir();
        this->~MenuPrin();

    }while(!salir);
}
