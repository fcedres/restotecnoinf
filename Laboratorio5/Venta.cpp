#include "Venta.h"

Venta::Venta(){
	this->idVenta=0;
	this->factura = nullptr;
	this->colVentaProducto.clear();
}


Venta::Venta(int idVenta, Factura* factura){
	this->idVenta=idVenta;
	this->factura = factura;
	this->colVentaProducto.clear();
}

int Venta::getidVenta(){
    return idVenta;
}

void Venta::setidVenta(int idVenta){
    this->idVenta=idVenta;
}

bool Venta::tieneFactura(){

	bool resultado = true;

	if (this->factura==nullptr){
		resultado = false;
	}
	return resultado;
}

void Venta::eliminarProducto(int IdProducto){

}

bool Venta::existeProducto(int IdProducto){

	int idP = IdProducto;
	bool existe = false;
	list<VentaProducto*> aux = this->colVentaProducto;

	for (list<VentaProducto*>::iterator it=aux.begin(); it!=aux.end(); ++it){
		VentaProducto* vp = *it;
		if (idP == vp->obtenerIdProducto()){
			existe = true;
		}
	}
	return existe;
}

Venta::~Venta()=default;

