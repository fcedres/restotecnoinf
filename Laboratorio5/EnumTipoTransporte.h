/*
 * EnumTipoTransporte.h
 *
 *  Created on: 06/06/2020
 *      Author: eduardo
 */

#ifndef LABORATORIO5_ENUMTIPOTRANSPORTE_H_
#define LABORATORIO5_ENUMTIPOTRANSPORTE_H_


#ifndef PAV2020_LAB1_ENUMTIPOBICI_H
#define PAV2020_LAB1_ENUMTIPOBICI_H
enum tipoTransporte{pie,bici,moto};
#endif //PAV2020_LAB1_ENUMTIPOBICI_H


#endif /* LABORATORIO5_ENUMTIPOTRANSPORTE_H_ */
