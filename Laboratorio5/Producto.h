/*
 * Producto.h
 *
 *  Created on: 06/06/2020
 *      Author: eduardo
 */

#ifndef LABORATORIO5_PRODUCTO_H_
#define LABORATORIO5_PRODUCTO_H_

#include <string>
using namespace std;

class Producto{
private:
	int idProducto;
	string descripcion;
	float precio;

public:
	//constructor
	Producto();
	Producto(int idProducto, string descripcion , float precio);

	//seters
	string getDescripcion();
	int getIdProducto();
	float getPrecio();

	//geters
	void setIdProducto(int idProducto);
	void setPrecio(float precio);
	void setDescripcion(const string descripcion);
    virtual void agregarP (int idProducto, int cantidad);

	//destructor
	virtual ~Producto();
};



#endif /* LABORATORIO5_PRODUCTO_H_ */
