/*
 * Repartidor.cpp
 *
 *  Created on: 06/06/2020
 *      Author: eduardo
 */

#include "Repartidor.h"

Repartidor::Repartidor():Empleado(){
	this->transporte=pie;
	this->colVentasD;//.clear();
}

Repartidor::Repartidor(int idEmpleado,string nombre, tipoTransporte transporte):Empleado(idEmpleado,nombre){
	this->transporte=transporte;
	this->colVentasD;
}

/*int Repartidor::getIdEmpleado(){
	return this->idEmpleado;
}
string Repartidor::getNombre(){
	return this->nombre;
}*/

/*void Repartidor::setIdEmpleado(int idEmpleado){
	this->idEmpleado=idEmpleado;
}
void Repartidor::setNombre(string nombre){
	this->nombre=nombre;
}*/
tipoTransporte Repartidor::getTransporte(){
	return this->transporte;
}

void Repartidor::setTransporte(tipoTransporte transporte){
	this->transporte=transporte;
}

Repartidor::~Repartidor()=default;

