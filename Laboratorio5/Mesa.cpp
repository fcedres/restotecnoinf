//
// Created by Fabio on 8/6/2020.
//

#include "Mesa.h"

//Constructores
Mesa::Mesa() {
    this->numero = 0;
    this->colLocal.clear();
    this->local = nullptr;
}

//Mesa::Mesa(int numero){
//	this->numero=numero;
//}

Mesa::Mesa(int numero, Local* local){

    this->numero = numero;
    this->local = local;

}

//Seters
void Mesa::setNumero(int numero) {
    this->numero = numero;
}

//Geters
int Mesa::getNumero() {
    return this->numero;
}

//Destructor
Mesa::~Mesa() = default;

bool Mesa::tieneVentaEnCurso(){
	bool resultado = true;

	if (this->local==nullptr){
		resultado = false;
	}
	return resultado;
}

bool Mesa::atendidaPorMozo(int idEmpleado){
	return false;
}

int Mesa::setLocal(Local* local){
	this->local=local;
	return 0;
}

//bool Mesa::mesaSinVenta(){ // hace lo mismo que tieneVentaEnCurso
//	return false;
//}
