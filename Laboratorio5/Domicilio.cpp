#include "Domicilio.h"

Domicilio::Domicilio():Venta(){
	//this->cliente=DtCliente();
}
Domicilio::Domicilio(int idVenta, Factura* factura, DtCliente cliente):Venta(idVenta,factura){
	this->cliente=cliente;
}

DtCliente Domicilio::getDtCliente(){
	return this->cliente;
}

void Domicilio::setDtCliente(DtCliente cliente){
	this->cliente=cliente;
}

Domicilio::~Domicilio()=default;

