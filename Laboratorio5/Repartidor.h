/*
 * Repartidor.h
 *
 *  Created on: 06/06/2020
 *      Author: eduardo
 */

#ifndef LABORATORIO5_REPARTIDOR_H_
#define LABORATORIO5_REPARTIDOR_H_

#include "Empleado.h"
#include "Domicilio.h"
#include "EnumTipoTransporte.h"
#include <map>
#include <string>
using namespace std;

//class Bicicleta: public Vehiculo{
class Repartidor: public Empleado{
private:
	tipoTransporte transporte;
	map<int,Domicilio*> colVentasD;

public:
	//constructor
	Repartidor();
	Repartidor(int idEmpleado, string nombre, tipoTransporte transporte);

	//geters
//	int getIdEmpleado();
//	string getNombre();
	tipoTransporte getTransporte();

	//geters
//	void setIdEmpleado(int idEmpleado);
//	void setNombre(string nombre);
	void setTransporte(tipoTransporte transporte);

	//destructor
	~Repartidor();
};
#endif /* LABORATORIO5_REPARTIDOR_H_ */
