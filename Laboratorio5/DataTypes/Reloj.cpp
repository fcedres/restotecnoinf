/*
 * Reloj.cpp
 *
 *  Created on: 11/06/2020
 *      Author: andres
 */

#include "Reloj.h"

Reloj* Reloj::instancia = nullptr;
//Constructores
//Reloj::Reloj(){
//    time_t now = time(0);
//    tm *ltm = localtime(&now);
//    this->dia = ltm->tm_mday;
//    this->mes = 1 + ltm->tm_mon;
//    this->anio = 1900 + ltm->tm_year;
//    this->hora = ltm->tm_hour;
//    this->minuto = ltm->tm_min;
//}

Reloj* Reloj::getInstancia() {

    if( instancia == nullptr){
        instancia = new Reloj();
    }
    return  instancia;
}

//Reloj::Reloj(int dia, int mes, int anio, int hora, int minuto) {
//    this->dia = dia;
//    this->mes = mes;
//    this->anio = anio;
//    this->hora = hora;
//    this->minuto = minuto;
//}

//Seters
void Reloj::setDia(int dia) {
    this->dia = dia;
}

void Reloj::setMes(int mes){
    this->mes = mes;
}

void Reloj::setAnio(int anio){
    this->anio = anio;
}

void Reloj::setHora(int hora){
    this->hora = hora;
}

void Reloj::setMinuto(int minuto) {
    this->minuto = minuto;
}

//Geters
int Reloj::getDia() {
    return this->dia;
}

int Reloj::getMes() {
    return this->mes;
}

int Reloj::getAnio() {
    return this->anio;
}

int Reloj::getHora() {
    return this->hora;
}

int Reloj::getMinuto() {
    return this->minuto;
}

//Destructor
Reloj::~Reloj(){}