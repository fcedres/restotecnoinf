//
// Created by Fabio on 8/6/2020.
//

#include "DtDireccion.h"

DtDireccion::DtDireccion(){
    this->nombreCalle = "";
    this->numeroPuerta = 0;
    this->esquina1 = "";
    this->esquina2 = "";
}

DtDireccion::DtDireccion(string nombreCalle, int numeroPuerta, string esquina1, string esquina2){
    this->nombreCalle = nombreCalle;
    this->numeroPuerta = numeroPuerta;
    this->esquina1 = esquina1;
    this->esquina2 = esquina2;
}

//Getters
string DtDireccion::getNombreCalle(){
    return this->nombreCalle;
}
int DtDireccion::getNumeroPuerta(){
    return this->numeroPuerta;
}

string DtDireccion::getEsquina1() {
    return this->esquina1;
}

string DtDireccion::getEsquina2() {
    return this->esquina2;
}

DtDireccion::~DtDireccion() = default;
