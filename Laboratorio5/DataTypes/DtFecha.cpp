/*
 * DtFecha.cpp
 *
 *  Created on: 11/06/2020
 *      Author: andres
 */

#include "DtFecha.h"
//Constructores
DtFecha::DtFecha() {

    Reloj* reloj = Reloj::getInstancia();

    this->dia = reloj->getDia();
    this->mes = reloj->getMes();
    this->anio = reloj->getAnio();
    this->hora = reloj->getHora();
    this->minuto = reloj->getMinuto();
}

DtFecha::DtFecha(int dia, int mes, int anio, int hora, int minuto) {

    this->dia = dia;
    this->mes = mes;
    this->anio = anio;
    this->hora = hora;
    this->minuto = minuto;
}

//Geters
int DtFecha::getDia() {
    return this->dia;
}

int DtFecha::getMes() {
    return this->mes;
}

int DtFecha::getAnio() {
    return this->anio;
}

int DtFecha::getHora() {
    return this->hora;
}

int DtFecha::getMinuto() {
    return this->minuto;
}

//Destructor
DtFecha::~DtFecha() = default;