//
// Created by Fabio on 8/6/2020.
//

#ifndef LABORATORIO5_DTPRODUCTO_H
#define LABORATORIO5_DTPRODUCTO_H
using namespace std;

#include <string>

class DtProducto {

private:
    int idProducto;
    string descripcion;
    float precio;

public:
    //Constructor
    DtProducto();
    DtProducto(int idProducto, string descripcion, float precio);

    //Getters
    int getIdProducto();
    string getDescripcion();
    float getPrecio();

    virtual ~DtProducto();
};
#endif //LABORATORIO5_DTPRODUCTO_H
