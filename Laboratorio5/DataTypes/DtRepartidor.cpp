/*
 * DtRepartidor.cpp
 *
 *  Created on: 06/06/2020
 *      Author: eduardo
 */

#include "DtRepartidor.h"

DtRepartidor::DtRepartidor():DtEmpleado(){
	this->transporte=pie;
}

DtRepartidor::DtRepartidor(int idEmpleado,string nombre,tipoTransporte transporte):DtEmpleado(idEmpleado,nombre){
	this->transporte=transporte;
}

tipoTransporte DtRepartidor::getTransporte(){
	return this->transporte;
}

DtRepartidor::~DtRepartidor()=default;


