/*
 * DtFactura.h
 *
 *  Created on: 09/06/2020
 *      Author: Alejandro
 */

#ifndef Laboratorio5_DtFactura_h
#define Laboratorio5_DtFactura_h

#include "DtProductoVendido.h"
#include "DtFecha.h"
#include <string>
#include <map>
using std::string;

class DtFactura{
private:
    int idVenta;
    string nombre;
    DtFecha fecha;
    map<int, DtProductoVendido*> productos;
    float subtotal;
    float descuento;
    float montoTotal;
    float precioTotal;

public:
	//constructor
	DtFactura();
	DtFactura(int idVenta,string nombre, DtFecha fecha, float subtotal, float descuento, float montoTotal, float precioTotal);

	//geters
    int getidVenta();
    string getnombre();
    DtFecha getfecha();
    map<int, DtProductoVendido*> getProductos();
    float getsubtotal();
    float getdescuento();
    float getmontototal();
    float getprecioTotal();


	//destructor
	~DtFactura();

};



#endif


