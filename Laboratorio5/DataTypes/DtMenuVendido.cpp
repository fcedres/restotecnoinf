
/*
 * DtMenuVendido.cpp
 *
 *  Created on: 11/06/2020
 *      Author: andres
 */

//#include "DtProductoVendido.h"
#include "DtMenuVendido.h"
//Contructores


DtMenuVendido::DtMenuVendido():DtProducto(){
	this->cantProdMenu=0;
}

DtMenuVendido::DtMenuVendido(int idProducto, string descripcion, float precio, int cantProdMenu):DtProducto(idProducto,descripcion,precio){
	this->cantProdMenu=cantProdMenu;
}

	//Seters

    //Geters
/*

	int DtMenuVendido::getIdProducto(){
        return this->idProducto;
    }

	float DtMenuVendido::getPrecio(){
        return this->precio;
    }

	string DtMenuVendido::getDescripcion(){
        return this->descripcion;
    }
*/

    int DtMenuVendido::getCantProdMenu(){
        return this->cantProdMenu;
    }

    //Destructor
    DtMenuVendido::~DtMenuVendido() = default;



