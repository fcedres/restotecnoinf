/*
 * DtMesa.h
 *
 *  Created on: 09/06/2020
 *      Author: Alejandro
 */

#ifndef Laboratorio5_DtMesa_h
#define Laboratorio5_DtMesa_h

#include <string>
using std::string;

class DtMesa{
private:
    int numero;

public:
	//constructor
	DtMesa();
	DtMesa(int numero);

	//geters
    int getnumero();

	//destructor
	~DtMesa();

};



#endif



