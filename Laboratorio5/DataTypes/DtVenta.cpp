//
// Created by Fabio on 8/6/2020.
//

#include "DtVenta.h"

DtVenta::DtVenta(){
    this->idVenta = 0;
}

DtVenta::DtVenta(int idVenta){
    this->idVenta = idVenta;
}

//Getters
int DtVenta::getIdVenta(){
    return this->idVenta;
}

DtVenta::~DtVenta() = default;