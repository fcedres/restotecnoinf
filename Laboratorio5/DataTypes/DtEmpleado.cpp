/*
 * DtEmpleado.cpp
 *
 *  Created on: 06/06/2020
 *      Author: eduardo
 */

#include "DtEmpleado.h"

DtEmpleado::DtEmpleado(){
	this->idEmpleado=0;
	this->nombre="";
}

DtEmpleado::DtEmpleado(int idEmpleado, string nombre){
	this->idEmpleado=idEmpleado;
	this->nombre=nombre;
}

int DtEmpleado::getIdEmpleado(){
	return this->idEmpleado;
}

string DtEmpleado::getNombre(){
	return this->nombre;
}

DtEmpleado::~DtEmpleado(){}


