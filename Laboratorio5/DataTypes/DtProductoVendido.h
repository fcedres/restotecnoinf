/*
 * DtProductoVendido.h
 *
 *  Created on: 06/06/2020
 *      Author: eduardo
 */

#ifndef LABORATORIO5_DATATYPES_DTPRODUCTOVENDIDO_H_
#define LABORATORIO5_DATATYPES_DTPRODUCTOVENDIDO_H_

#include "DtProducto.h"
#include <string>
using std::string;

class DtProductoVendido: public DtProducto{
private:
	int cantidad;
public:
	DtProductoVendido();
	DtProductoVendido(int idProducto, float precio, string descripcion, int cantidad);
	int getIdProducto();
	float getPrecio();
	string getDescripcion();
	int getCantidad();
};
#endif /* LABORATORIO5_DATATYPES_DTPRODUCTOVENDIDO_H_ */
