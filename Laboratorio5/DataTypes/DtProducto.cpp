//
// Created by Fabio on 8/6/2020.
//

#include "DtProducto.h"

DtProducto::DtProducto(){
    this->idProducto = 0;
    this->descripcion = "";
    this->precio = 0.0;
}

DtProducto::DtProducto(int idProducto, string descripcion, float precio){
    this->idProducto = idProducto;
    this->descripcion = descripcion;
    this->precio = precio;
}

//Getters
int DtProducto::getIdProducto(){
    return this->idProducto;
}
string DtProducto::getDescripcion(){
    return this->descripcion;
}

float DtProducto::getPrecio(){
    return this->precio;
}


DtProducto::~DtProducto() = default;