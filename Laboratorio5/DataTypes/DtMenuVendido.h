
/*
 * DtMenuVendido.h
 *
 *  Created on: 11/06/2020
 *      Author: andres
 */

#ifndef LABORATORIO5_DATATYPES_DTMENUVENDIDO_H_
#define LABORATORIO5_DATATYPES_DTMENUVENDIDO_H_

#include "DtProducto.h"
#include <string>
using std::string;
using namespace std;

class DtMenuVendido: public DtProducto{
private:
	int cantProdMenu;
public:

	//Contructores
	DtMenuVendido();
	DtMenuVendido(int idProducto, string descripcion, float precio, int cantProdMenu);

	//Seters

	//Geters
	/*int getIdProducto();
	float getPrecio();
	string getDescripcion();*/
	int getCantProdMenu();

	//destructor
	~DtMenuVendido();
};
#endif /* LABORATORIO5_DATATYPES_DTMENUVENDIDO_H_ */
