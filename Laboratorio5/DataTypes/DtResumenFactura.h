/*
 * DtResumenFactura.h
 *
 *  Created on: 06/06/2020
 *      Author: eduardo
 */

#ifndef LABORATORIO5_DATATYPES_DTRESUMENFACTURA_H_
#define LABORATORIO5_DATATYPES_DTRESUMENFACTURA_H_

#include "DtFactura.h"
#include <map>

class DtResumenFactura{
private:
	float resumenMonto;
	map<int,DtFactura*> colFacturas;

public:
	//constructor
	DtResumenFactura(float resumenMonto);

	//geters
	map<int, DtFactura*> getFacturas();
	float getResumenMonto();

	//Destructor
	~DtResumenFactura();

};
#endif /* LABORATORIO5_DATATYPES_DTRESUMENFACTURA_H_ */
