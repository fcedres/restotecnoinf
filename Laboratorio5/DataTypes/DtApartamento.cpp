//
// Created by Fabio on 8/6/2020.
//

#include "DtApartamento.h"

//Constructor
DtApartamento::DtApartamento() : DtDireccion() {
    this->nombreEdificio = "";
    this->nroApto = 0;
}

DtApartamento::DtApartamento(string nombreCalle, int numeroPuerta, string esquina1, string esquina2, string nombreEdificio, int nroApto)
				:DtDireccion(nombreCalle,numeroPuerta,esquina1,esquina2){
    this->nombreEdificio = nombreEdificio;
    this->nroApto = nroApto;
}

//Getters
string DtApartamento::getNombreEdificio(){
    return this->nombreEdificio;
}

int DtApartamento::getNroApto(){
    return this->nroApto;
}

//Destructor
DtApartamento::~DtApartamento() = default;
