/*
 * DtMenu.cpp
 *
 *  Created on: 17/06/2020
 *      Author: eduardo
 */

#include "DtMenu.h"

DtMenu::DtMenu():DtProducto(){
	this->colMenuComun.clear();
}

DtMenu::DtMenu(int idProducto, string descripcion, float precio) : DtProducto (idProducto, descripcion, precio){

}

map<int, MenuComun*> DtMenu::obtenerMenuProducto(){
	return this->colMenuComun;
}

void DtMenu::setearMapa(map<int,MenuComun*> mapa){
	this->colMenuComun=mapa;
}

DtMenu::~DtMenu(){
}

