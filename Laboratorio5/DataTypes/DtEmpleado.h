/*
 * DtEmpleado.h
 *
 *  Created on: 06/06/2020
 *      Author: eduardo
 */

#ifndef LABORATORIO5_DATATYPES_DTEMPLEADO_H_
#define LABORATORIO5_DATATYPES_DTEMPLEADO_H_

#include <string>
using std::string;

class DtEmpleado{
private:
	int idEmpleado;
	string nombre;
public:
	//constructor
	DtEmpleado();
	DtEmpleado(int idEmpleado, string nombre);

	//seters
	int getIdEmpleado();
	string getNombre();
	virtual ~DtEmpleado();
};



#endif /* LABORATORIO5_DATATYPES_DTEMPLEADO_H_ */
