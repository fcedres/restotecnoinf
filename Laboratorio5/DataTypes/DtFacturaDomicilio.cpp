/*
 * DtFacturaDomicilio.cpp
 *
 *  Created on: 09/06/2020
 *      Author: Alejandro
 */

#include "DtFacturaDomicilio.h"

DtFacturaDomicilio::DtFacturaDomicilio():DtFactura(){
	this->transporte=pie;
}

DtFacturaDomicilio::DtFacturaDomicilio(int idVenta,string nombre, DtFecha fecha, float subtotal, float descuento, float montoTotal, float precioTotal, tipoTransporte transporte)
:DtFactura(idVenta,nombre,fecha,subtotal,descuento,montoTotal,precioTotal){
	this->transporte=transporte;
}

tipoTransporte DtFacturaDomicilio::getTransporte(){
	return this->transporte;
}

DtFacturaDomicilio::~DtFacturaDomicilio()=default;



