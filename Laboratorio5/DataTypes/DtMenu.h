/*
 * DtMenu.h
 *
 *  Created on: 17/06/2020
 *      Author: eduardo
 */

#ifndef DATATYPES_DTMENU_H_
#define DATATYPES_DTMENU_H_

#include "DtProducto.h"
#include <map>;
#include "../MenuComun.h"
#include <iostream>
using namespace std;

class DtMenu: public DtProducto {
private:
	map<int, MenuComun*> colMenuComun;
public:
	DtMenu();
	DtMenu(int idProducto, string descripcion, float precio);
	map<int, MenuComun*> obtenerMenuProducto();
	void setearMapa(map<int,MenuComun*> mapa);


	virtual ~DtMenu();
};

#endif /* DATATYPES_DTMENU_H_ */
