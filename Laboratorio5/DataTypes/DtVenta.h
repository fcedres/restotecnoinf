//
// Created by Fabio on 8/6/2020.
//

#ifndef LABORATORIO5_DTVENTA_H
#define LABORATORIO5_DTVENTA_H
using namespace std;

#include <string>

class DtVenta {

private:
    int idVenta;

public:
    //Constructor
    DtVenta();
    DtVenta(int idVenta);

    //Getters
    int getIdVenta();

    virtual ~DtVenta();
};
#endif //LABORATORIO5_DTVENTA_H
