//
// Created by Fabio on 8/6/2020.
//

#ifndef LABORATORIO5_DTDIRECCION_H
#define LABORATORIO5_DTDIRECCION_H
using namespace std;

#include <string>

class DtDireccion {

private:
    string nombreCalle;
    int numeroPuerta;
    string esquina1;
    string esquina2;

public:
    //Constructor
    DtDireccion();
    DtDireccion(string nombreCalle, int numeroPuerta, string esquina1, string esquina2);

    //Getters
    string getNombreCalle();
    int getNumeroPuerta();
    string getEsquina1();
    string getEsquina2();

    virtual ~DtDireccion();
};
#endif //LABORATORIO5_DTDIRECCION_H
