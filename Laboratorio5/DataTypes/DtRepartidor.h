/*
 * DtRepartidor.h
 *
 *  Created on: 06/06/2020
 *      Author: eduardo
 */

#ifndef LABORATORIO5_DATATYPES_DTREPARTIDOR_H_
#define LABORATORIO5_DATATYPES_DTREPARTIDOR_H_

#include "DtEmpleado.h"
#include "../EnumTipoTransporte.h"
#include <string>
using std::string;

class DtRepartidor: public DtEmpleado{
private:
	tipoTransporte transporte;

public:
	//constructor
	DtRepartidor();
	DtRepartidor(int idEmpleado, string nombre, tipoTransporte transporte);

	//geters
	/*int getIdEmpleado();
	string getNombre();*/
	tipoTransporte getTransporte();

	//destructor
	~DtRepartidor();

};



#endif /* LABORATORIO5_DATATYPES_DTREPARTIDOR_H_ */
