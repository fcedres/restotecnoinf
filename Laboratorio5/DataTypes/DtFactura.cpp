/*
 * DtFactura.cpp
 *
 *  Created on: 09/06/2020
 *      Author: Alejandro
 */

#include "DtFactura.h"

DtFactura::DtFactura(){
	this->idVenta=0;
	this->nombre="";
	this->fecha;
	this->productos;
	this->subtotal=0;
	this->descuento=0;
	this->montoTotal=0;
	this->precioTotal=0;
}

DtFactura::DtFactura(int idVenta,string nombre, DtFecha fecha, float subtotal, float descuento, float montoTotal, float precioTotal){
	this->idVenta=idVenta;
	this->nombre=nombre;
	this->fecha=fecha;
	this->productos=productos;
	this->subtotal=subtotal;
	this->descuento=descuento;
	this->montoTotal=montoTotal;
	this->precioTotal=precioTotal;
}

int DtFactura::getidVenta(){
    return this->idVenta;
}

string DtFactura::getnombre(){
    return this->nombre;
}

DtFecha DtFactura::getfecha(){
    return this->fecha;
}

map<int, DtProductoVendido*> DtFactura::getProductos(){
    return this->productos;
}

float DtFactura::getsubtotal(){
    return this->subtotal;
}

float DtFactura::getdescuento(){
    return this->descuento;
}

float DtFactura::getmontototal(){
    return this->montoTotal;
}

float DtFactura::getprecioTotal(){
    return this->precioTotal;
}

DtFactura::~DtFactura()=default;



