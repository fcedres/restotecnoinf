/*
 * Reloj.h
 *
 *  Created on: 11/06/2020
 *      Author: andres
 */

#ifndef IMPLEMENTACION_RELOJ_H
#define IMPLEMENTACION_RELOJ_H

#include <ctime>
#include <sstream>
#include <cstdlib>

using namespace std;

class Reloj {

private:
    static Reloj* instancia;
    Reloj(){

        time_t now = time(0);
        tm *ltm = localtime(&now);

        this->dia = ltm->tm_mday;
        this->mes = 1 + ltm->tm_mon;
        this->anio = 1900 + ltm->tm_year;
        this->hora = ltm->tm_hour;
        this->minuto = ltm->tm_min;
    }
    Reloj(Reloj const&){};
    int dia;
    int mes;
    int anio;
    int hora;
    int minuto;

public:
    //constructor
//    Reloj();
    static Reloj* getInstancia();

//    Reloj(int dia, int mes, int anio, int hora, int minuto);

    //Seters
    void setDia(int dia);

    void setMes(int mes);

    void setAnio(int anio);

    void setHora(int hora);

    void setMinuto(int minuto);

    //Geters
    int getDia();

    int getMes();

    int getAnio();

    int getHora();

    int getMinuto();

    //Destrucot
    ~Reloj();

};


#endif //IMPLEMENTACION_RELOJ_H
