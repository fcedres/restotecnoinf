//
// Created by Fabio on 8/6/2020.
//

#ifndef LABORATORIO5_DTLOCAL_H
#define LABORATORIO5_DTLOCAL_H

#include "DtVenta.h"


class DtLocal: public DtVenta {

private:

public:
    //Constructor
    DtLocal();
    DtLocal(int idVenta);

    //Getters


    //Destructor
    ~DtLocal();
};

#endif //LABORATORIO5_DTLOCAL_H
