/*
 * DtFecha.h
 *
 *  Created on: 11/06/2020
 *      Author: andres
 */

#ifndef DTFECHA_H
#define DTFECHA_H


#include <ctime>
//#include <iostream>
#include <sstream>
#include <cstdlib>
#include "Reloj.h"

using namespace std;

class DtFecha {

private:
    int dia;
    int mes;
    int anio;
    int hora;
    int minuto;

public:
    //constructor
    DtFecha();

    DtFecha(int dia, int mes, int anio, int hora, int minuto);

    //Geters
    int getDia();

    int getMes();

    int getAnio();

    int getHora();

    int getMinuto();

    //Destructor
    ~DtFecha();
};

#endif //DTFECHA_H
