/*
 * DtCliente.h
 *
 *  Created on: 06/06/2020
 *      Author: eduardo
 */

#ifndef LABORATORIO5_DATATYPES_DTCLIENTE_H_
#define LABORATORIO5_DATATYPES_DTCLIENTE_H_

#include "DtDireccion.h"
#include <string>
using std::string;

class DtCliente{
private:
	int telefono;
	string nombre;
	DtDireccion* direccion;

public:
	//constructor
	DtCliente();
	DtCliente(int telefono,string nombre,DtDireccion* direccion);

	//geters
	DtDireccion* getDireccion();
	string getNombre();
	int getTelefono();
};
#endif /* LABORATORIO5_DATATYPES_DTCLIENTE_H_ */
