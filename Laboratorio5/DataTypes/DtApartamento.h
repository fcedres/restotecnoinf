//
// Created by Fabio on 8/6/2020.
//

#ifndef LABORATORIO5_DTAPARTAMENTO_H
#define LABORATORIO5_DTAPARTAMENTO_H
#include "DtDireccion.h"


class DtApartamento: public DtDireccion {

private:
    string nombreEdificio;
    int nroApto;

public:
    //Constructor
    DtApartamento();
    DtApartamento(string nombreCalle, int numeroPuerta, string esquina1, string esquina2, string nombreEdificio, int nroApto);

    //Getters
    string getNombreEdificio();
    int getNroApto();

    //Destructor
    ~DtApartamento();
};
#endif //LABORATORIO5_DTAPARTAMENTO_H
