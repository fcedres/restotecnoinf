/*
 * DtFacturaDomicilio.h
 *
 *  Created on: 09/06/2020
 *      Author: Alejandro
 */

#ifndef Laboratorio5_DtFacturaDomicilio_h
#define Laboratorio5_DtFacturaDomicilio_h

#include "DtFactura.h"
#include "../EnumTipoTransporte.h"
#include <string>
using std::string;

class DtFacturaDomicilio: public DtFactura{
private:

    tipoTransporte transporte;

public:
	//constructor
	DtFacturaDomicilio();
	DtFacturaDomicilio(int idVenta,string nombre, DtFecha fecha, float subtotal, float descuento, float montoTotal, float precioTotal, tipoTransporte transporte);

	//geters
    tipoTransporte getTransporte();

	//destructor
	~DtFacturaDomicilio();

};



#endif



