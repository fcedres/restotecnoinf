/*
 * DtDomicilio.h
 *
 *  Created on: 09/06/2020
 *      Author: Alejandro
 */

#ifndef Laboratorio5_DtDomicilio_h
#define Laboratorio5_DtDomicilio_h

#include "DtVenta.h"
#include "DtCliente.h"
#include <string>
#include <map>
using std::string;

class DtDomicilio: public DtVenta{
private:
    map<int, DtCliente*> clientes;

public:
	//constructor
	DtDomicilio();
	DtDomicilio(int idVenta);

	//geters

	map<int, DtCliente*> getClientes();

	//destructor
	~DtDomicilio();

};



#endif

