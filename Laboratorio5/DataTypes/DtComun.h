//
// Created by Fabio on 9/6/2020.
//

#ifndef LABORATORIO5_DTCOMUN_H
#define LABORATORIO5_DTCOMUN_H
#include "DtProducto.h"


class DtComun: public DtProducto {

private:

public:
    //Constructor
    DtComun();
    DtComun(int idProducto, string descripcion, float precio);

    //Getters

    //Destructor
    ~DtComun();
};
#endif //LABORATORIO5_DTCOMUN_H
