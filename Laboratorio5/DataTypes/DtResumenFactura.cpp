/*
 * DtResumenFactura.cpp
 *
 *  Created on: 06/06/2020
 *      Author: eduardo
 */

#include "DtResumenFactura.h"

DtResumenFactura::DtResumenFactura(float resumenMonto){
	this->resumenMonto=resumenMonto;
	this->colFacturas;
}

map<int, DtFactura*> DtResumenFactura::getFacturas(){
	return this->colFacturas;
}

float DtResumenFactura::getResumenMonto(){
	return this->resumenMonto;
}
