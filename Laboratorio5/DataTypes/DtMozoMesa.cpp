//
// Created by Fabio on 8/6/2020.
//

#include "DtMozoMesa.h"

//Constructor
DtMozoMesa::DtMozoMesa() {
    this->idEmpleado = 0;
    this->colMesas.clear();
}

DtMozoMesa::DtMozoMesa(int idEmpleado, list<int> mesas){
    this->idEmpleado = idEmpleado;

    for(list<int>::iterator it=mesas.begin();it!= mesas.end(); ++it){

    //    Mesa* mesa = new Mesa(it->getIdMesa());
    //    this->colMesas.insert(make_pair(it->getIdMesa(),mesa));   ////////this->coleccionSalas.insert(make_pair(it->getCapacidad(),sala));////////ERA ESTO
    }
}

//Getters
int DtMozoMesa::getIdEmpleado(){
    return this->idEmpleado;
}

list<int> DtMozoMesa::listarMesas() {
    list<int> datosMesas;
    for (map<int,Mesa*>::iterator it= this->colMesas.begin(); it!= this->colMesas.end(); it++)
    {
        int mesa = it->second->getNumero();
        datosMesas.push_front(mesa);
    }
    return datosMesas;
}

//Destructor
DtMozoMesa::~DtMozoMesa() = default;
