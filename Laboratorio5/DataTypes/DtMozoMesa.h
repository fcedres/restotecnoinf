//
// Created by Fabio on 8/6/2020.
//

#ifndef LABORATORIO5_DTMOZOMESA_H
#define LABORATORIO5_DTMOZOMESA_H

#include "list"
#include "../Mesa.h"
#include "../Mozo.h"
using namespace std;

class DtMozoMesa {

private:
    int idEmpleado;
    //set(int) mesas;
    map<int, Mesa*> colMesas;

public:
    //Constructor
    DtMozoMesa();
    DtMozoMesa(int idEmpleado, list<int> mesas);

    //Getters
    int getIdEmpleado();
    list<int> listarMesas();

    //Destructor
    ~DtMozoMesa();
};
#endif //LABORATORIO5_DTMOZOMESA_H
