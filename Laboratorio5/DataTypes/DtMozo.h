/*
 * DtMozo.h
 *
 *  Created on: 15/06/2020
 *      Author: eduardo
 */

#ifndef DATATYPES_DTMOZO_H_
#define DATATYPES_DTMOZO_H_

#include "DtEmpleado.h"

class DtMozo: public DtEmpleado {
public:
	DtMozo();
	DtMozo(int idEmpleado, string nombre);

	virtual ~DtMozo();
};

#endif /* DATATYPES_DTMOZO_H_ */
