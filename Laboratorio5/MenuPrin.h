//
// Created by Fabio on 3/6/2020.
//

#ifndef LABORATORIO5_MENUPRIN_H
#define LABORATORIO5_MENUPRIN_H

#include "Controladores/CPersona.h"
#include "Controladores/CProducto.h"
#include "Controladores/CVentaFacturaMesa.h"
#include "Fabricas/FCPersona.h"
#include "Fabricas/FCProducto.h"
#include "Fabricas/FCVentaFacturaMesa.h"
#include "Interfaces/ICPersona.h"
#include "Interfaces/ICProducto.h"
#include "Interfaces/ICVentaFacturaMesa.h"
#include "DataTypes/DtMozo.h"



#include <iostream>
#include "iomanip"
#include <list>
#include <cstdio>
#include <string>
#include <stdlib.h>
#include <time.h>
using namespace std;
//#include "Reloj.h"

class MenuPrin{
private:
    //instancia de Menu
    static MenuPrin* instancia;
//    bool usuarioLogIn; BORRAR SEBA

    // Constructor.
    MenuPrin();

    // Interfases y fabricas
    ICProducto* IPC;
    FCProducto* FPC;
    ICVentaFacturaMesa* IVFMC;
    FCVentaFacturaMesa* FVFMC;
    ICPersona* IPerC;
    FCPersona* FPerC;

public:
    // Obtener la instancia del menu.
    static MenuPrin* getInstancia();

    // Impresion e interaccion del menu.
    void impMenuMozo();
    void impMenuRepartidor();
    void impMenuAdmin();
    void impMenuCliente();
    void cargarDatosDePrueba();
    void MenuListarEmpleados();
    void MenuListarClientes();
    void MenuListarProductos();
    void MenuListarRepartidores();
    void menuBajaProducto();
    void MenuInformacionDeProductos();
    void menuListarMesas();
    void menuAsignarMozoAMesa();
    void menuVentaDomicilio();
    void mMenuPrin();
    void optSalir();
};



#endif //LABORATORIO5_MENUPRIN_H
