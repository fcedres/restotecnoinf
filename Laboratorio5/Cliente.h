/*
 * Cliente.h
 *
 *  Created on: 11/06/2020
 *      Author: andres
 */

#ifndef CLIENTE_H_
#define CLIENTE_H_
#include "DataTypes/DtDireccion.h"
class Cliente{
private:
	int telefono;
	string nombre;
    DtDireccion* direccion;

public:
    //Constructores
    Cliente();
    Cliente(int telefono, string nombre, DtDireccion* direccion);

    //Seters

    void setTelefono(int telefono);
    void setNombre(string nombre);

   // void setDireccion(DtDireccion direccion);

   void setDireccion(DtDireccion* direccion);


    //Geters
    int getTelefono();
    string getNombre();

    //DtDireccion getDireccion();

    DtDireccion* getDireccion();



    //Destructor
    ~Cliente();


};


#endif /* CLIENTE_H_ */



