/*
 * Producto.cpp
 *
 *  Created on: 06/06/2020
 *      Author: eduardo
 */

#include "Producto.h"

Producto::Producto(){
	this->descripcion="";
	this->idProducto=0;
	this->precio=0;
}

Producto::Producto(int idProducto, string descripcion, float precio){
	this->idProducto=idProducto;
	this->descripcion=descripcion;
	this->precio=precio;
}

string Producto::getDescripcion(){
	return this->descripcion;
}

void Producto::setDescripcion(const string descripcion) {
	this->descripcion = descripcion;
}

int Producto::getIdProducto(){
	return this->idProducto;
}

void Producto::setIdProducto(int idProducto) {
	this->idProducto = idProducto;
}

float Producto::getPrecio(){
	return this->precio;
}

void Producto::setPrecio(float precio) {
	this->precio = precio;
}

void Producto::agregarP (int idProducto, int cantidad){

}


Producto::~Producto()=default;
