/*
 * Factura.h
 *
 *  Created on: 10/06/2020
 *      Author: Alejandro
 */

 #include "Mozo.h"
#include "Factura.h"

Factura::Factura(){
    this->codigoVenta=0;
    this->fecha= DtFecha();
    this->productos.clear();
    this->subTotalVenta=0;
    this->precioTotal=0;
}


Factura::Factura(int codigoVenta, DtFecha fecha, float subTotalVenta, float precioTotal){
    this->codigoVenta=codigoVenta;
    this->fecha=fecha;
    this->productos;
    this->subTotalVenta=subTotalVenta;
    this->precioTotal=precioTotal;
}

int Factura::getcodigoVenta(){
    this->codigoVenta;
}

void Factura::setcodigoVenta(int codigoVenta){
    this->codigoVenta=codigoVenta;
}

DtFecha Factura::getfecha(){
    this->fecha;
}

void Factura::setfecha(DtFecha fecha){
    this->fecha=fecha;
}

float Factura::getsubTotalVenta(){
    return this->subTotalVenta;
}

void Factura::setsubTotalVenta(float subTotalVenta){
    this->subTotalVenta=subTotalVenta;
}

float Factura::getprecioTotal(){
    return this->precioTotal;
}

void Factura::setprecioTotal(float precioTotal){
    this->precioTotal=precioTotal;
}

Factura::~Factura()=default;
